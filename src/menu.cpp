#include "menu.hpp"
#include "Apply.hpp"
#include <algorithm>
#include <cmath>

// basically menu interface for each valikko/splash thingy and god knows what

Menu::Menu(const sf::Font& fon,int charSize) :
    rndr(),
    spr(),
    selection(),
    view(),
    font(fon),
    menuList(),
	active(1),
    characterSize(charSize),
    clicks(-1),
    bgColor(sf::Color(255, 60, 0))
{
    selection.setFillColor(sf::Color(255, 94, 0));
	if(linep == -1)
    {
        linep = font.getLineSpacing(characterSize)-characterSize; // we need to pad lines for menus
    }
}
void Menu::draw(sf::RenderTarget &target)
{
    target.draw(spr);

}

void Menu::create(float width,float height)
{
    if(height == 0)
        height = (float) menuList.size()*(linep+characterSize);
    else
        height = (float) std::min((int) menuList.size()*(linep+characterSize), (int) (linep+characterSize)); // I am not sure about this menu height definition, hopefully it works here

    //max width of the sf::Texts of the menu items
    float maxwidth = (**std::max_element(menuList.begin(),menuList.end(),
                            [](menupointer& a,menupointer &b){ return a->getLocalBounds().width<b->getLocalBounds().width;})).getLocalBounds().width;

    if(width == 0)  width = maxwidth;
    else
        width = std::max(width,maxwidth);
    rndr.clear(bgColor);
    rndr.create(width,height);
    view = rndr.getDefaultView();
    selection.setSize(sf::Vector2f(width,linep+characterSize));


    rndr.clear(bgColor);
    rndr.draw(selection);
    for(const auto &r : menuList)
    {
        rndr.draw(*r);
    }
    rndr.display();
    spr.setTexture(rndr.getTexture(),true);
}
void Menu::clear() //clear menulist for initializing another one
{
    menuList.clear();
}
void Menu::add(const std::string& item,sf::Color textColor) // add menu options
{
    menupointer newText(new sf::Text(item,font,characterSize));
    newText->setColor(textColor);
    newText->setPosition(0,menuList.size()*(linep+characterSize));
    menuList.push_back(std::move(newText));
}

void Menu::updateTexture() //highlights the texture
{
    selectionBg.setPosition(0,marker*(linep+characterSize));
 
    rndr.setView(view);
    rndr.clear(bgColor);
    rndr.draw(selection);
    for(const auto &m : menuList)
    {
        rndr.draw(*m);
    }
    rndr.display();
}

void Menu::pass(const sf::Event &event) //event passing for clicks
{
    if(!active) return;

    if(event.type == sf::Event::MouseButtonPressed)
    {
        sf::Vector2f mouse(event.mouseButton.x,event.mouseButton.y);
        sf::Vector2f mouseInView = rndr.mapPixelToCoords(sf::Vector2i(mouse) - sf::Vector2i(spr.getPosition()));
        if(spr.getGlobalBounds().contains(mouse) && mouseInView.y/(linep+characterSize) != marker && mouseInView.y/(linep+characterSize) <= menuList.size())
        {
            clicks = marker = mouseInView.y/(linep+characterSize);
			updateTexture();
        }
    }

}

