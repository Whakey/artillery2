/* 
 * File:   Stronghold.cpp
 * Author: iivari
 * 
 * Created on November 29, 2015, 11:58 PM
 */

#include <SFML/Graphics/Sprite.hpp>
#include <ciso646>

#include "Stronghold.hpp"

Stronghold::Stronghold(sf::Vector2i location, Player &owner) : location(location), owner(owner) {
    health = 100;
    maxhealth = 100;
    broken_t.loadFromFile("Stronghold_broken.png");
    unbroken_t.loadFromFile("Stronghold_unbroken.png");
    castle.setTexture(unbroken_t);
}

void Stronghold::makeToCastle() {
    if (castle.getScale().x != scale) {
        castle.setOrigin(100, 200);
        castle.setScale(scale, scale);
    } else if (health <= 0 && castle.getTexture() == &unbroken_t) {
        castle.setTexture(broken_t);
    }
}

void Stronghold::update() {
    makeToCastle();
    castle.setPosition(location.x, location.y);

}

bool Stronghold::heathChange(int change) {
    if (destroyed)
        return true;
    health += change;
    if (health > maxhealth) health = maxhealth;
    if (health <= 0 and !destroyed) {
        destroyed = true;
        health = 0;       
        owner.addDeath();
        return false;
    }
    return true;
}

void Stronghold::augmentMaxhealth(int amounth) {
    maxhealth += amounth;
}
