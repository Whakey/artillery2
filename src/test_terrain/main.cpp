#include <SFML/Graphics.hpp>
#include <stdlib.h> 
#include <ctime>
#include <iostream>
#include <random>
#include "../Terrain.hpp"

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

void mySleep(int sleepMs)
{
#ifdef LINUX
	usleep(sleepMs * 1000);   // usleep takes sleep time in us (1 millionth of a second)
#endif
#ifdef WINDOWS
	Sleep(sleepMs);
#endif
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "Terrain");
	
	sf::Vector2i size(1000, 1000);
	Terrain background(size);
	background.generateTerrain(10, 300);
	background.refresh();

	sf::Image sky;
	sky.create(size.x, size.y, sf::Color(0, 191, 255, 255));
	sf::Texture skytxtr;
	skytxtr.loadFromImage(sky);
	sf::Sprite skysprite;
	skysprite.setTexture(skytxtr);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space) {
				std::cout << "Generating new terrain..." << std::endl;
				background.generateTerrain(15, 200);
			}

			if (event.type == sf::Event::MouseButtonPressed)
			{
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					std::clock_t startexpl;
					startexpl = std::clock();
					background.explosion(sf::Vector2i(event.mouseButton.x, event.mouseButton.y), 50);
					std::cout << "Time for explosion: " << (std::clock() - startexpl) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;

					std::clock_t startrefresh;
					startrefresh = std::clock();
					background.refresh(sf::Vector2i(event.mouseButton.x, event.mouseButton.y), 50);
					std::cout << "Time to refresh terrain: " << (std::clock() - startrefresh) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
					std::cout << "Height of terrain at x you clicked at: " << background.terrainHeight(sf::Vector2i(event.mouseButton.x, event.mouseButton.y)).y << std::endl;
				}
			}
		}

		window.clear();
		window.draw(skysprite);
		background.updateSprite();
		window.draw(background.getSprite());
		window.display();
	}
	return 0;
}
