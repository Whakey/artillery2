#ifndef SPLASH_HPP
#define SPLASH_HPP

#include "Appstates.hpp"
#include "menu.hpp"

class Splash : public AppStates
{
    public:
        Splash();
        ~Splash();
        void renew();
        void draw(sf::RenderWindow &window);
        void update(float deltat);
        void pass(sf::Event event);
    private:
        sf::Sprite bg;
        Menu mainw;
};

#endif // SPLASH_HPP
