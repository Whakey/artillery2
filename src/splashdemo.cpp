
#include <stdio.h>

// additional headers
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <map>
#include <iostream>
#include <cassert>

#include "splashdemo.h"

void Splash::Show(sf::RenderWindow & win)
{
	sf::Texture img;
	if(img.loadFromFile("../splash.jpg") != true)
	{
		return;
	}

	sf::Sprite sprite(img);
	
	win.draw(sprite);
        
        
	win.display();

	sf::Event eevee;
	while(1)
	{
		while(win.pollEvent(eevee))
		{
			if((eevee.type == sf::Event::EventType::KeyPressed)
				|| (eevee.type == sf::Event::EventType::MouseButtonPressed)
				|| (eevee.type == sf::Event::EventType::Closed) )
			{
				return;
			}
		}
	}
}
