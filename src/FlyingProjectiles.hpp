/* 
 * File:   FlyingProjectiles.hpp
 * Author: iivari
 *
 * Created on November 9, 2015, 11:44 PM
 */

#ifndef FLYINGPROJECTILES_HPP
#define	FLYINGPROJECTILES_HPP

#include "Projectile.hpp"
#include <list>
#include <memory>
#include <set>
#include "SoundList.hpp"

class FlyingProjectiles {
public:

    FlyingProjectiles(unsigned int height, SoundList& voicelist) : height(height), voicelist(voicelist) {
    }
    //    FlyingProjectiles(const FlyingProjectiles& fp);
    ~FlyingProjectiles();

    const std::list< std::shared_ptr<Projectile>>&getFlyingProjectiles() const {
        return projectiles;
    }
    
    SoundList& getVoicelist() {
      return voicelist;
    }
    
    size_t get_size() {
        return projectiles.size();
    }

    
    void addProjectile(sf::Vector2f, sf::Vector2f, const ProjectileItem&, int);
    void addProjectile(sf::Vector2i, sf::Vector2f, const ProjectileItem&, int );
    void divideProjectile(Projectile&);
    int deleteAndExplodeUselesProjectiles (std::list<std::shared_ptr<Stronghold>>, Terrain&, Stronghold&);
    void draw(sf::RenderWindow &window, Terrain&);

    Projectile& operator[](const size_t i);

private:
    std::list<std::shared_ptr<Projectile>> projectiles;
    unsigned int height;
    SoundList& voicelist;

};
bool compareProjectiles(Projectile p1, Projectile p2);


#endif	/* FLYINGPROJECTILES_HPP */

