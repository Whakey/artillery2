#include "Setup.hpp"
#include "Apply.hpp"

Setup::Setup() :
	AppStates(SetupState),
    bgm(Apply::getTexture(Splashbg)),
    set_up(Apply::getFont(SkipLegDay),40),
	Plyrs(2)
{
	set_up.add("2P",sf::Color::Red);
	set_up.add("3P",sf::Color::Red);
	set_up.add("4P",sf::Color::Red);
    set_up.add("Exit",sf::Color::Red);
    set_up.setSelectionColor(sf::Color::Yellow);
    set_up.create(800,0);
    set_up.setPosition(0,300);
}

Setup::~Setup()
{

}
void Setup::renew()
{}


void Setup::update(float deltat)
{

    int q = set_up.querySelection();
    if(q >= 0)
    {
        switch(q)
        {
        case 0: //2P
			Apply::changeState(GameState);
			Apply::getGame().newGame(2);
            break;
        case 1://3P
			Apply::changeState(GameState);
			Apply::getGame().newGame(3);
            break;
        case 2: //4P
			Apply::changeState(GameState);
			Apply::getGame().newGame(4);
            break;
		case 4://Exit
            Apply::changeState(SplashState);
			Apply::getTitle();
            break;
        }
    }


 
}

void Setup::pass(sf::Event event)
{
    set_up.pass(event);
}
