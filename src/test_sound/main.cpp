/* 
 * File:   main.cpp
 * Author: hopsale
 *
 * Created on November 10, 2015, 5:20 PM
 */

#include <SFML/Audio.hpp>

#include "../SoundList.hpp"
#include <iostream>

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

// Using sounds in your code, write #include "Game.hpp" and then following,
// where you would like to play the sound: Game::getVoiceList().playSound(TYPE);
// where TYPE could be for example CLICK or EXPLOSION
void someFunction(SoundList &lista) {
    lista.playSound(SHOOT3);
    
}

void someFunction2(SoundList &lista) {
    lista.playSound(EXPLOSION);
    
}

void someFunction3(SoundList &lista) {
    lista.playSound(PURCHASE);
    
}

int main() {
    
    SoundList list = SoundList();
    list.initializeSounds();
    list.playSound(WINNING);
    sf::sleep(sf::seconds(5));
    list.playAllSounds();
    
    std::shared_ptr<Sound> sound1 = std::make_shared<Sound>("../Sounds/shoot3.ogg", SHOOT3);
    std::shared_ptr<Sound> sound2 = std::make_shared<Sound>("../Sounds/explosion.wav", EXPLOSION);
    std::shared_ptr<Sound> sound3 = std::make_shared<Sound>("../Sounds/purchase.wav", PURCHASE);
    
    SoundList voicelist = SoundList();
    
    voicelist.addSound(sound1);
    voicelist.addSound(sound2);
    voicelist.addSound(sound3);
    
    someFunction(voicelist);
    sf::sleep(sf::seconds(2));
    
    // Will play same time
    someFunction2(voicelist);
    someFunction3(voicelist);    
    sf::sleep(sf::seconds(5));

    return 0;
    

}

