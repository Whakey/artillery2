#include <stdio.h>

// additional headers
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <map>
#include <iostream>
#include <cassert>


#include "gamedemo.h"


//this function = the entry point
int main(int argc, const char* argv[])
{
	Game::Start();
	
	return 0;
}