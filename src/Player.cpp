/* 
 * File:   Player.cpp
 * Author: iivari
 * 
 * Created on November 9, 2015, 2:32 PM
 */

#include "Player.hpp"

Player::Player(sf::Texture avatar, std::string name, bool is_ai, unsigned int money)
: avatar(avatar), name(name), is_ai(is_ai), money(money) {
    deaths = 0;
    kills = 0;
    caused_havoc = 0;
}

bool Player::changeMoneyAmount(int change) {
    if (money + change < 0) return false;
    money += change;
    return true;
}




