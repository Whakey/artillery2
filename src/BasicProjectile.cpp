/* 
 * File:   BasicProjectile.cpp
 * Author: iivari
 * 
 * Created on November 10, 2015, 3:35 AM
 */

#include <memory>
#include <vector>

#include "BasicProjectile.hpp"
#include "BasicExplosion.hpp"
#include "ExplosionType.hpp"

BasicProjectile::BasicProjectile() : ProjectileItem(0, 0, 0, true, 3, 40, 5000, std::make_shared<BasicExplosion>()) {
    //int max_departures, int max_projectiles_in divide, int price, bool ai_is_able_to_buy, float radius, float explosion_radius, float mass, std::make_shared<BasicExplosion>
    std::vector<sf::Vector2f> dpv;
    dpv.push_back(sf::Vector2f(0,0));
    
    std::vector<sf::Vector2f> dvv;
    
    dvv.push_back(sf::Vector2f(0,0));
    
    setDisplacements(dpv);
    setDivergeVelocitys(dvv);
    
    setFillAndOutColor(sf::Color::Black, sf::Color::Black);
    
    
    setName("Basic projectile");
    setSoundType(EXPLOSION);
}


