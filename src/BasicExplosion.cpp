/* 
 * File:   BasicExplosion.cpp
 * Author: iivari
 * 
 * Created on November 12, 2015, 10:57 PM
 */

#include <SFML/Graphics/Sprite.hpp>

#include "BasicExplosion.hpp"
#include <math.h>

BasicExplosion::BasicExplosion():ExplosionType(10) {
    setExplosionColor(sf::Color::Yellow);
}


int BasicExplosion::simulateExplosionDamage(sf::Vector2f location, Stronghold victim , float radius) const {
    auto dist = pow(pow(location.x - victim.getLocation().x,2) + pow(location.y - location.y,2), 0.5);
    if (dist > radius) return 0;
    return round (100*(radius-dist)/radius);
}
