#ifndef GAME_HPP
#define GAME_HPP
#include "TrajectoryManager.hpp"
#include "FlyingProjectiles.hpp"
#include "Terrain.hpp"
#include "Appstates.hpp"
#include "SoundList.hpp"
#include "HUD.hpp"
#include <cassert>
#include <memory>
#include <list>
#include <string>

class Game : public AppStates
{
public:
    Game();
    void renew();
    void draw(sf::RenderWindow &window);
    void pass(sf::Event Event);
    void update(float deltat);

    void newGame(int n_players);
	bool isPlayerTurn(Stronghold current);


private:
	Terrain background;
	int Curplayerint; // turn order
	std::vector<Stronghold *> strongholds;
	std::list<std::shared_ptr<Player>> players;
	FlyingProjectiles f_projectiles(int size);
	float wind;
	SoundList voicelist;
	HUD healthplayer(int getPlayerIndex, int healthbar);
	int healthmax;
	std::string str;
	float power;
    float angle;

};

#endif // GAME_HPP
