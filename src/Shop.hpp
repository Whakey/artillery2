/* 
 * File:   Shop.hpp
 * Author: iivari
 *
 * Created on November 18, 2015, 6:54 PM
 */

#ifndef SHOP_HPP
#define	SHOP_HPP

#include "ProjectileItem.hpp"
#include <string>
#include <map>
#include <memory>

class Shop {
public:
    Shop();
    Shop(const Shop& orig);
    virtual ~Shop();
    void add(const std::string&, std::shared_ptr<ProjectileItem>);
    ProjectileItem& operator[](const std::string&);
    ProjectileItem& buyProjectileItem(Player&, const std::string&);
    bool canPlayerBuy(const Player&, const std::string&);
    void buyMoreHealth(Stronghold&, int);
    
private:
    std::map<std::string, std::shared_ptr<ProjectileItem>> types;
    int money_used;

};

#endif	/* SHOP_HPP */

