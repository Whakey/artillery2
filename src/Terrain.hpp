#ifndef TERRAIN_H
#define TERRAIN_H

#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include "Spline.hpp"

class Terrain {

public:
	Terrain(sf::Vector2i size);

	//Load the terrain from .bmp file, returns True if it worked
	bool terrainFromFile(const std::string &filename);

	//Drop hanging pixels down to ground
	void refresh();
	//refresh that doesnt look at useless pixels
	void refresh(sf::Vector2i explpos, int explradius);
	//helper function for refresh, returns the next black pixel below pos
	sf::Vector2i nextBlackPixel(sf::Vector2i pos);

	//create triangle like map
	void triangleFill();
	//midpoint displacement
	void generateTerrain(int segments, int maxdisplace);

	void explosion(sf::Vector2i pos, int radius);
	void updateSprite();

	bool isGround(sf::Vector2i pos);

	sf::Vector2i terrainHeight(sf::Vector2i pos);

	sf::Sprite const& getSprite() const;
	sf::Image const& getImage() const;

private:
	sf::Vector2i size;
	sf::Image image;
	sf::Texture texture;
	sf::Sprite sprite;
};

#endif 