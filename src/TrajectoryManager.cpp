/* 
 * File:   TrajectoryManager.cpp
 * Author: iivari
 * 
 * Created on November 10, 2015, 12:13 AM
 */

#include "TrajectoryManager.hpp"
#include <math.h>
#include <vector>
#include "SoundList.hpp"



void TrajectoryManager::UpdateProjectiles(FlyingProjectiles& projectiles) {
    auto projectilesvec = projectiles.getFlyingProjectiles();
    for (auto it : projectiles.getFlyingProjectiles()) {
        if (it->getPosition().y <= height) {
            //std::clock_t start;
            //start = std::clock();
            ChangeNextVelocityLocation(*it);
            //std::cout << "Time to refresh projectile: " << (std::clock() - start) / (double) (CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
        }
    }
    std::vector<Projectile> projectiles2;
    for (auto it = projectilesvec.begin(); it != projectilesvec.end(); it++) {
        auto projectile = (*it)->getType().canMakeToProjectiles((*it)->getPosition(), (*it)->getVelocity(), (*it)->getDivideCount());
        if (projectile) {
            (*it)->addDivide();
            projectiles2.push_back(**it);
        }
    }
    for (auto it : projectiles2) {
        projectiles.divideProjectile(it);
    }
    
}

// FlyingProjectiles TrajectoryManager::getResult(Stronghold s, float power, float angle, const ProjectileItem& type) {
//     FlyingProjectiles projectiles(height);
// 
//     projectiles.addProjectile(sf::Vector2f(s.getLocation().x, s.getLocation().y - 200*0.2), convertToVelocity(power, angle), type, 0);
//     bool changer = true;
//     while (changer == true) {
//         changer = 0;
//         for (auto it : projectiles.getFlyingProjectiles()) {
//             if (it->getPosition().y <= height && !terrain.isGround(it->getPositionInt())) {
//                 changer = 1;
//             }
//         }
//         if (changer == 0) {
//             return projectiles;
//         } else {
//             UpdateProjectiles(projectiles);
//         }
//     }
//     return projectiles;
// 
// }

void TrajectoryManager::ChangeNextVelocityLocation(Projectile & projectile) {
    if (!terrain.isGround(projectile.getPositionInt())) {
        float odx = projectile.getVelocity().x; 
        const float C = 0.2;
        const float Pi = 3.14;
        const float r = projectile.getType().getRadius();
        float fx = pow((odx - wind), 2) * Pi * r * r * C;
        if (odx > wind) {
            fx *= -1;
        }

        float g = 9.81;
        float mass = projectile.getType().getMass();
        float x = 0.5 * fx * pow(timestep, 2) / mass + odx * timestep;
        float y = 0.5 * g * pow(timestep, 2) + projectile.getVelocity().y * timestep;
        float dx = fx * timestep / mass;
        float dy = g * timestep;
        projectile.changePosition(sf::Vector2f(x, y));
        projectile.changeVelocity(sf::Vector2f(dx, dy));
    }
}

sf::Vector2f TrajectoryManager::convertToVelocity(float power, float angle) {
    return sf::Vector2f(-1 * power * sin(angle), power * cos(angle));
}

