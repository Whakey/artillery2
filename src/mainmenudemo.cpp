#include <stdio.h>

// additional headers
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <map>
#include <iostream>
#include <cassert>

#include "mainmenudemo.h"

MainMenu::Result MainMenu::Show(sf::RenderWindow& win)
{

	//lataa img
	sf::Texture image;
	image.loadFromFile("../demomenu.jpg"); //kaytetaan menyykuvaa joka tulee kohta tahan
	sf::Sprite sprite(image);

	//Setup clickables

	MenuItem pButton;
	pButton.getrekt.top= 145;
	pButton.getrekt.width = 380;
	pButton.getrekt.left = 0;
	pButton.getrekt.height = 799;
	pButton.act = Play;


	MenuItem eButton;
	eButton.getrekt.left = 0;
	eButton.getrekt.height = 799;
	eButton.getrekt.top = 383;
	eButton.getrekt.width = 333;
	eButton.act = Exit;

	_menuItms.push_back(pButton);
	_menuItms.push_back(eButton);

	win.draw(sprite);
	win.display();

	return GetMenuRes(win);
}

MainMenu::Result MainMenu::HandleClick(int x, int y)
{
	std::list<MenuItem>::iterator it;

	for ( it = _menuItms.begin(); it != _menuItms.end(); it++)
	{
		sf::Rect<int> menuItemRect = (*it).getrekt;
		if( menuItemRect.width > y && menuItemRect.top < y && menuItemRect.left < x && menuItemRect.height > x)
			{
				return (*it).act;
			}
	}

	return None;
}

MainMenu::Result  MainMenu::GetMenuRes(sf::RenderWindow& win)
{
	sf::Event menuevent;

	while(1) //menyy pyorii niin kauan kunnes jotain tapahtuu...
	{

		while(win.pollEvent(menuevent))
		{
			if(menuevent.type == sf::Event::MouseButtonPressed)
			{
			// KLIK KLIK TAHAN
				return HandleClick(menuevent.mouseButton.x,menuevent.mouseButton.y);
			}
			if(menuevent.type == sf::Event::Closed)
			{
				return Exit;
			}
		}
	}
}
