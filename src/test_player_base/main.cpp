/* 
 * File:   main.cpp
 * Author: iivari
 *
 * Created on November 9, 2015, 2:31 PM
 */

#include <SFML/Graphics/Texture.hpp>
#include <SFML/System.hpp>
#include "../Player.hpp"
#include "../Stronghold.hpp"
#include <iostream>

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

int main() {
    sf::Texture texture;
    std::vector<Player*> players;
    std::cout << "Add some players" << std::endl << std::endl;
    Player *player1 = new Player(texture, "Iivari", false, 100);
    Player *player2 = new Player(texture, "Ville", false, 100);
    Player *player3 = new Player(texture, "Alexander", false, 100);
    Player *player4 = new Player(texture, "Matvej", false, 100);
    players.push_back(player1);
    players.push_back(player2);
    players.push_back(player3);
    players.push_back(player4);

    for (auto it : players) {
        std::cout << it->getName() << " money: " << it->getMoney() << std::endl;
    }

    std::cout << std::endl << "Add some bases" << std::endl << std::endl;

    sf::Vector2i location(50, 50);
    std::vector<Stronghold*> bases;
    Stronghold *base1 = new Stronghold(location, *player1);
    Stronghold *base2 = new Stronghold(location, *player2);
    Stronghold *base3 = new Stronghold(location, *player3);
    Stronghold *base4 = new Stronghold(location, *player4);
    bases.push_back(base1);
    bases.push_back(base2);
    bases.push_back(base3);
    bases.push_back(base4);



    for (auto it : bases) {
        std::cout << it->getOwner().getName() << " health: " << it->getHealth() << "/" << it->getMaxthealth() << std::endl;
    }
    
    std::cout << std::endl << "base1 shoots and hits base2" << std::endl;
    base1->getOwner().addHavoc(110);
    base2->heathChange(-110);
    base1->getOwner().changeMoneyAmount(50);
    base1->getOwner().addKill();
    base2->getOwner().addDeath();
    std::cout << "player1 uses money to buy more maxhealt" << std::endl << std::endl;
    base1->augmentMaxhealth(20);
    
    
    for (auto it : bases) {
        std::cout << it->getOwner().getName() << " health: " << it->getHealth() << "/" << it->getMaxthealth() << " is destroyd: " << it->isDestroyed() <<" Kills: " << it->getOwner().getKills() << " Deaths: " << it->getOwner().getDeaths() << std::endl;
    }
    

    return 0;

}

