/* 
 * File:   main.cpp
 * Author: iivari
 *
 * Created on November 10, 2015, 1:56 AM
 */


#include <SFML/Graphics/Texture.hpp>
#include <SFML/System.hpp>


#include "../FlyingProjectiles.hpp"
#include "../BasicProjectile.hpp"
#include "../ExplosionType.hpp"
#include "../BasicExplosion.hpp"
#include "../DividingProjectile.hpp"
#include "../RainProjectile.hpp"
#include "../Shop.hpp"
#include "../SoundList.hpp"
#include <iostream>
#include <memory>

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

int main() {

;
    Shop shop;
    sf::Texture avatar;
    avatar.loadFromFile("BasicExplosion.jpg");
    Player p = Player(avatar, "iivari", false, 100);

    Stronghold b = Stronghold(sf::Vector2i(50, 50), p);

    std::cout << p.getName() << " money " << p.getMoney() << std::endl << p.getName() << " buys something from shop" << std::endl;
    shop.buyProjectileItem(p, "Divide");
    std::cout << p.getName() << " money " << p.getMoney() << std::endl;
    std::cout << "something hits " << b.getOwner().getName() << std::endl;
    b.heathChange(-22);
    std::cout << b.getOwner().getName() << " " << b.getHealth() << "/" << b.getMaxthealth() << std::endl;
    std::cout << p.getName() << " buys more health with 30 dollars" << std::endl;
    shop.buyMoreHealth(b, 30);
    std::cout << b.getOwner().getName() << " monney: " << p.getMoney() << " health: " << b.getHealth() << "/" << b.getMaxthealth() << std::endl;

    return 0;
}

