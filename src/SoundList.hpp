/* 
 * File:   SoundList.hpp
 * Author: hopsale
 *
 * Created on November 12, 2015, 9:36 PM
 */

#ifndef SOUNDLIST_HPP
#define	SOUNDLIST_HPP

#include "Sound.hpp"
#include <vector>

class SoundList {
public:
    
    SoundList();
    
    void addSound(std::shared_ptr<Sound> sound);
    std::vector<std::shared_ptr<Sound>>::iterator findSound(SoundType type);
    
    void initializeSounds();
    void playSound(SoundType type);
    void playAllSounds();
    int getLength();
    
    std::string getMainPath() {
        return mainpath;
    }
    
    //void removeSound(SoundType type);
    //void setSounds(std::vector<std::pair<std::string, SoundType>> listOfSounds);
    
    
private:
    std::vector<std::shared_ptr<Sound>> sounds;
    int size;
    std::vector<std::shared_ptr<Sound>>::iterator it;
    std::vector<std::pair<std::string, SoundType>>::iterator it2;
    std::string mainpath = "../Sounds/";
};

#endif	/* SOUNDLIST_HPP */

