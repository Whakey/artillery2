/* 
 * File:   Sound.cpp
 * Author: hopsale
 * 
 * Created on November 11, 2015, 8:54 PM
 */

#include "Sound.hpp"

Sound::Sound(std::string filename, SoundType type, std::string location) : 
    type(type), location(location) {
    
    if (!buffer.loadFromFile(filename))
        std::cout << "could not load sound from file " << filename << std::endl;
    sound.setBuffer(buffer);
    length = 1 / buffer.getSampleRate() * buffer.getSampleCount();
}
