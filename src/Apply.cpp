#include "Apply.hpp"
#include "Resourcesids.hpp"
#include <iostream>

// done by Matvej
void Apply::start()
{
    main.create(sf::VideoMode(800,600),"Kanuunalierot 3000");
    main.setFramerateLimit(110); //framerate
	main.setVerticalSyncEnabled(false);
// Activating vertical synchronization will limit the number of frames displayed to the refresh rate of the monitor. This can avoid some visual artifacts, and limit the framerate to a good value (but not constant across different computers).
    sf::Clock frametimer;
    changeState(SplashState);
	currentState->renew();
    while(main.isOpen())
    {
        sf::Event event;
        while(main.pollEvent(event))
        {
            if(event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
            {
                main.close();
                return;
            }
            else
				currentState->pass(event);
        }
        main.clear(sf::Color::Black);
		currentState->draw(main);
        main.display();
        sf::Time dt = frametimer.getElapsedTime();
        frametimer.restart();
        currentState->update(dt.asSeconds());
    }
}







void Apply::changeState(AppStateType rump)
{
    stackofstates.push_back(rump);
	currentState = getState(rump);
	//change the game state - simple as that
}


void Apply::quit(void)
{
    main.close();
}



AppStates* Apply::getState(AppStateType state)
{
    switch(state)
    {
	case SetupState:
        return &someSetup;
    case SplashState:
        return &Title;
    case GameState:
        return &someGame;
	case GameOverState:
        return &someGameOver;

    }
    return nullptr; // since NULL would be too ambiguous
	// we want to define where we are atm
}

sf::Texture& Apply::getTexture(TextureIdentifier id)
{
    if(!Loaded)
        resauces();
    return textures.get(id);
}
sf::Font& Apply::getFont(FontIdentifier id)
{
    if(!Loaded)
        resauces();
    return fonts.get(id);
}

void Apply::resauces()
{
    if(Loaded)
        return;
    if( !(textures.load(Splashbg,"data/splash.jpg") &&
    fonts.load(SkipLegDay,"data/SkipLegday.ttf") &&
    fonts.load(UbuntuCondensed,"data/Ubuntu-C.ttf")) )
        throw std::runtime_error("resource fail");
    else
       Loaded = true;

}

//ordering has to be intact/exact

Textures Apply::textures;
Fonts Apply::fonts;
bool Apply::Loaded{false};
std::deque<AppStateType> Apply::stackofstates;
sf::RenderWindow Apply::main;
Game Apply::someGame;
GameOver Apply::someGameOver;
Splash Apply::Title;
Setup Apply::someSetup;
AppStates* Apply::currentState;
int main()
{
    srand(time(NULL));
    Apply::resauces();
	Apply::start();
    return 0;
}
