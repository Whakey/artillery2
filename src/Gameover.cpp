#include "Gameover.hpp"
#include "Apply.hpp"

GameOver::GameOver() :
    AppStates(GameOverState),
    bground(),
    bgsprite(),
    gameovertext("Game Over!",Apply::getFont(SkipLegDay)){
    gameovertext.setColor(sf::Color::Black);
    gameovertext.setCharacterSize(40);
    gameovertext.setOrigin(gameovertext.getLocalBounds().width/2,gameovertext.getLocalBounds().height/2);

}


void GameOver::pass(sf::Event Event)
{
    if((Event.type == sf::Event::KeyPressed) || (Event.type == sf::Event::MouseButtonReleased))
        Apply::changeState(SetupState);
}

void GameOver::renew()
{
    sf::RenderWindow &render = Apply::getWindow();
    bground.create(render.getSize().x,render.getSize().y);
    bground.update(render);
    bgsprite.setTexture(bground);
    gameovertext.setPosition(render.getSize().x/2,2*gameovertext.getCharacterSize());

}

void GameOver::draw(sf::RenderWindow &window)
{
    window.draw(bgsprite);
    window.draw(gameovertext);
}



void GameOver::update(float deltat){}