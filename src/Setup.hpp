#ifndef SETUP_HPP
#define SETUP_HPP
// done by Matvej
#include "Appstates.hpp"
#include "menu.hpp"

class Setup : public AppStates
{
    public:
        Setup();
        virtual ~Setup();
		virtual void update(float deltat);
        virtual void pass(sf::Event event);
        virtual void renew();
        virtual void draw(sf::RenderWindow &window);
    private:
        Menu set_up;
        int Plyrs;
        sf::Sprite bgm;
};

#endif // SETUP_HPP
