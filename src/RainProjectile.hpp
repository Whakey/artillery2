/* 
 * File:   RainProjectile.hpp
 * Author: iivari
 *
 * Created on November 25, 2015, 7:21 PM
 */

#ifndef RAINPROJECTILE_HPP
#define	RAINPROJECTILE_HPP

#include "ProjectileItem.hpp"
#include "BasicExplosion.hpp"
#include "ExplosionType.hpp"

class Projectile;

class RainProjectile : public ProjectileItem {
public:
    RainProjectile();

    virtual bool canMakeToProjectiles(const sf::Vector2f location, const sf::Vector2f velocity, int divide_count) const {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && getMaxDivideCount() > divide_count) return true;
        return false;
    }

private:
};


#endif	/* RAINPROJECTILE_HPP */

