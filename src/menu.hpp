#ifndef MENU_HPP
#define MENU_HPP
#include <SFML/Graphics.hpp>
#include <list>
#include <memory>

typedef std::unique_ptr<sf::Text> menupointer;

class Menu
{
public:
    Menu(const sf::Font &font,int charSize = 18);
    ~Menu();
    void draw(sf::RenderTarget &target);
    void add(const std::string& item,sf::Color textColor = sf::Color::Black);
    void step(float deltat);
    void create(float width = 0,float height = 0);
    void pass(const sf::Event& event);
    void clear();//clear and reset the menu items
    //clicks < 0 means not clicked yet, after querying it is set to -1
    inline int querySelection()
    {
        int temp = clicks;
        clicks=-1;
        return temp;
    }
    inline int getCharacterSize(){ return characterSize; }
    inline void setCharacterSize(int size){ characterSize=size;}
    inline void setPosition(float x,float y)
    {
        spr.setPosition(x,y);
    }
    inline void setSelectionColor(const sf::Color& theColor){ selection.setFillColor(theColor); }
    inline const sf::Color& getSelectionColor(){ return selection.getFillColor(); }
    inline sf::Vector2f getPosition(){ return spr.getPosition();}

    inline void setActive(bool val){ active = val; }
    inline bool getActive(){ return active; }
    inline const sf::Vector2f& getSize(){ return view.getSize(); }

private:
	void updateTexture();
    sf::RenderTexture rndr;
    sf::Sprite spr;
    sf::RectangleShape selection;
    sf::View view;
    const sf::Font &font;
    std::list<menupointer> menuList;
    bool active;
    int characterSize;
    int marker;
	int linep; 
    int clicks;//on item
    sf::Color bgColor;
};

#endif // MENU_HPP
