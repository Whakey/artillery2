#include "splash.hpp"
#include "Apply.hpp"

Splash::Splash() :
    AppStates(SplashState),
    bg(Apply::getTexture(Splashbg)),
    mainw(Apply::getFont(SkipLegDay),40)
{
	mainw.add("Play",sf::Color::Red);
    mainw.add("Quit",sf::Color::Red);
    mainw.setSelectionColor(sf::Color::Yellow);
    mainw.create(800,0);
    mainw.setPosition(0,300);
}

Splash::~Splash()
{

}
void Splash::renew()
{}


void Splash::update(float deltat)
{
    int q = mainw.querySelection();
    if(q >= 0)
    {
        switch(q)
        {
        case 0://Play
            Apply::changeState(SetupState);
            break;
        case 1://Exit
            Apply::quit();
            break;
        }
    }
}

void Splash::draw(sf::RenderWindow &window)
{
    window.draw(bg);
    mainw.draw(window);
}
void Splash::pass(sf::Event event)
{
    mainw.pass(event);
}
