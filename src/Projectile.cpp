#include "Projectile.hpp"
#include "SoundList.hpp"


bool Projectile::isInGroundOrOut(Terrain &terrain) {
    if (position.y <= 0) return false;
    if (position.y > 1000) return true;
    return (((unsigned) getPositionInt().y > terrain.getImage().getSize(). y) || terrain.isGround(getPositionInt()));
}

bool Projectile::isUseles(Terrain& terrain) {
    if (is_explosion) {
        frames_in_ground++;
    }
    if (frames_in_ground < getType().getExplosionType()->getLenghtInFrames())
        return false;
    return true;
}

void Projectile::makeToCircle() {
    if (is_explosion) {
        representing_circle.setFillColor(type.getExplosionType()->getExplosionColor());
        representing_circle.setRadius(0);
        representing_circle.setOrigin(0, 0);
        representing_circle.setOutlineThickness(0);
    } else {
        representing_circle.setFillColor(type.getFillColor());
        representing_circle.setOutlineColor(type.getOutColor());
        representing_circle.setRadius(type.getRadius());
        representing_circle.setOrigin(type.getRadius(), type.getRadius());
        representing_circle.setOutlineThickness(type.getRadius() / 4);
    }
}

void Projectile::draw(sf::RenderWindow &window, Terrain &terrain) {
    if (!has_been_drawed) {
        has_been_drawed = true;
        makeToCircle();
    } else if (is_explosion) {
        float radius = type.getExplosionRadius()*((float) frames_in_ground) / (float) type.getExplosionType()->getLenghtInFrames();
        representing_circle.setRadius(radius);
        representing_circle.setOrigin(radius, radius);
    } else if (!is_explosion && isInGroundOrOut(terrain)) {
        is_explosion = true;
        makeToCircle();
	voicelist.playSound(type.getSoundType());
    }
    representing_circle.setPosition(position);
    window.draw(representing_circle);
}

int Projectile::explodeProjectile(std::list<std::shared_ptr<Stronghold>> strongholds, Terrain& terrain, Stronghold& shooter) {
    int t_damage = 0;
    for (auto it : strongholds) {
        float damage = type.getExplosionType()->simulateExplosionDamage(position, *it, type.getExplosionRadius());
        if (damage != 0) {
            if (!it->heathChange(-damage)){
                shooter.getOwner().addKill();
	    }
	    shooter.getOwner().changeMoneyAmount(damage);
            shooter.getOwner().addHavoc(damage);
            t_damage += damage;
        }
    }
    //std::clock_t start;
    //start = std::clock();
    terrain.explosion(getPositionInt(), type.getExplosionRadius());
    terrain.refresh(getPositionInt(), type.getExplosionRadius());
    //std::cout << "Time to get refresh terrain: " << (std::clock() - start) / (double) (CLOCKS_PER_SEC / 1000) << " ms" << std::endl;



    for (auto it : strongholds) {
        it->updateLocation(terrain.terrainHeight(it->getLocation()));
    }
    return t_damage;
}