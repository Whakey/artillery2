/* 
 * File:   BasicExplosion.hpp
 * Author: iivari
 *
 * Created on November 12, 2015, 10:57 PM
 */

#ifndef BASICEXPLOSION_HPP
#define	BASICEXPLOSION_HPP

#include "ExplosionType.hpp"

class BasicExplosion : public ExplosionType {
public:
    BasicExplosion();

    virtual int simulateExplosionDamage(sf::Vector2f, Stronghold, float) const;

    virtual void makeDamage(sf::Vector2f, std::vector<Stronghold>&, float) {
    }
private:

};

#endif	/* BASICEXPLOSION_HPP */

