/* 
 * File:   WeakExplosion.cpp
 * Author: iivari
 * 
 * Created on December 9, 2015, 11:27 AM
 */

#include <SFML/Graphics/Color.hpp>

#include "WeakExplosion.hpp"
#include <math.h>


WeakExplosion::WeakExplosion(): ExplosionType(50) {
    setExplosionColor(sf::Color::Green);
}


int WeakExplosion::simulateExplosionDamage(sf::Vector2f location, Stronghold victim , float radius) const {
    auto dist = pow(pow(location.x - victim.getLocation().x,2) + pow(location.y - location.y,2), 0.5);
    if (dist > radius) return 0;
    return round (10*(radius-dist)/radius);
}


