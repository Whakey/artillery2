/* 
 * File:   FlyingProjectiles.cpp
 * Author: iivari
 * 
 * Created on November 9, 2015, 11:44 PM
 */


#include "FlyingProjectiles.hpp"
#include "TrajectoryManager.hpp"

//FlyingProjectiles::FlyingProjectiles(const FlyingProjectiles& fp) {
//    for (auto it : fp.projectiles) {
//        Projectile *p = it;
//        projectiles.push_back(p);
//    }
//}

FlyingProjectiles::~FlyingProjectiles() {
    projectiles.clear();
}

void FlyingProjectiles::draw(sf::RenderWindow &window, Terrain & terrain) {
    for (auto it : projectiles) {
        it->draw(window, terrain);
    }

}

int FlyingProjectiles::deleteAndExplodeUselesProjectiles(std::list<std::shared_ptr<Stronghold>> strongholds, Terrain & terrain, Stronghold & shooter) {
    int t_damage = 0;
    std::list<std::shared_ptr < Projectile>>::iterator i = projectiles.begin();
    while (i != projectiles.end()) {
        bool isActive = (*i)->isUseles(terrain);
        if (isActive) {
            t_damage += (*i)->explodeProjectile(strongholds, terrain, shooter);
            projectiles.erase(i++);
        } else {
            ++i;
        }
    }
    return t_damage;
}

void FlyingProjectiles::addProjectile(sf::Vector2f location, sf::Vector2f speed, const ProjectileItem& type, int divide_count) {
    if (get_size() < 700)
        projectiles.push_back(std::make_shared<Projectile>(location, speed, type, divide_count, voicelist));
}

void FlyingProjectiles::addProjectile(sf::Vector2i location, sf::Vector2f speed, const ProjectileItem& type, int divide_count) {
    projectiles.push_back(std::make_shared<Projectile>(sf::Vector2f(location.x, (location.y - 200 * 0.2)), speed, type, divide_count, voicelist));
}

void FlyingProjectiles::divideProjectile(Projectile& p) {
    int i = 0;
    while (p.getType().getMaxNewProjectilesInDivide() > i) {
        float dispx = p.getType().getDisplacement(i).x;
        float divex = p.getType().getDivergeVelocity(i).x;
        if (p.getVelocity().x < 0) {
            dispx *= -1;
            divex *= -1;
        }
        sf::Vector2f new_location = p.getPosition() + sf::Vector2f(divex, p.getType().getDisplacement(i).y);
        sf::Vector2f new_velocity = p.getVelocity() + sf::Vector2f(dispx, p.getType().getDivergeVelocity(i).y);
        addProjectile(new_location, new_velocity, p.getType(), p.getType().getMaxDivideCount());
        i++;
    }
}

Projectile& FlyingProjectiles::operator[](const size_t i) {
    if (i > projectiles.size()) {
        return **projectiles.end();
    }
    std::list<std::shared_ptr < Projectile>>::iterator it = projectiles.begin();
    std::advance(it, i);
    return (**it);
}



