#ifndef MAINMENUDEMO_H
#define	MAINMENUDEMO_H

#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include <list>

class MainMenu
{

public:
	enum Result { None, Exit, Play };	
	
	struct MenuItem
		{
		public:
			sf::Rect<int> getrekt;
			Result act;
		};
	
	Result Show(sf::RenderWindow& win);

private:
	Result GetMenuRes(sf::RenderWindow& win);
	Result HandleClick(int x, int y);
	std::list<MenuItem> _menuItms;
};



#endif	/* MAINMENUDEMO_H */
