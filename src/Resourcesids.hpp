#ifndef RESOURCSEIDS_INCLUDED
#define RESOURCESIDS_INCLUDED
#include <SFML/Graphics.hpp>
// done by Matvej
enum FontIdentifier
{
    SkipLegDay,
    UbuntuCondensed
};

enum TextureIdentifier
{
    Splashbg
};


template <typename Res, typename ID>
class Resources;

typedef Resources<sf::Texture,TextureIdentifier> Textures;
typedef Resources<sf::Font,FontIdentifier> Fonts;

// resource recognition mechanism

#endif // RESOURCESIDS_INCLUDED
