#include "HUD.hpp"
#include "Apply.hpp"
#include <cmath>
#include <string>


HUD::HUD(int getPlayerIndex, int healthbar) :

    player("",Apply::getFont(SkipLegDay),15),
	health("", Apply::getFont(SkipLegDay), 15){
    int xpadding = 30,ypadding = 30,lpadding = 5;
    int width = 800/3;
    int height = 15;
    //line 1
    player.setPosition  (xpadding + width*0,ypadding);
    player.setColor     (sf::Color::Black);
	player.setString("Player : " + playersindx.getOwner().getName();
	health.setPosition	(xpadding + width*1,ypadding);
	health.setColor		(sf::Color::Red);
	health.setString("Health : " + std::to_string(playersindx.getHealth()));
}


void HUD::stepper(Stronghold playersindx) {

	player.setString("Player : " + playersindx.getOwner().getName());
	health.setString("Health : " + std::to_string(playersindx.getHealth()));

}

void HUD::draw(sf::RenderWindow &win){
	win.draw(player);
	win.draw(health);
}


