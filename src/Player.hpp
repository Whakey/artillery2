/* 
 * File:   Player.hpp
 * Author: iivari
 *
 * Created on November 9, 2015, 2:32 PM
 */

#ifndef PLAYER_HPP
#define	PLAYER_HPP

#include <string>
#include <SFML/Graphics/Texture.hpp>

class Player {
public:
    Player(sf::Texture avatar, std::string name, bool is_ai, unsigned int monney);

    int getMoney() const {
        return money;
    }

    unsigned int getKills() const {
        return kills;
    }

    unsigned int getDeaths() const {
        return deaths;
    }

    unsigned int getCausedHavoc() const {
        return caused_havoc;
    }

    bool getIfAi() const {
        return is_ai;
    }

    std::string getName() {
        return name;
    }

    sf::Texture getAvatar() {
        return avatar;
    }

    void addHavoc(unsigned int havoc) {
        caused_havoc += havoc;
    }

    void addKill() {
        kills += 1;
    }

    void addDeath() {
        deaths += 1;
    }
    bool changeMoneyAmount(int);

private:
    sf::Texture avatar;
    std::string name;
    bool is_ai;
    int money;
    unsigned int deaths;
    unsigned int kills;
    unsigned int caused_havoc;


    //AI ai;


};



#endif	/* PLAYER_HPP */

