#ifndef APPLY_HPP
#define APPLY_HPP
#include <deque>
#include <string>
#include <SFML/Graphics.hpp>
// done by Matvej


#include "Resources.hpp"
#include "Game.hpp"
#include "Gameover.hpp"
#include "splash.hpp"
#include "Setup.hpp"

class Apply
{
public:
    Apply(); // delete? some usual application functions use "application = delete; but this syntaxes...
    static sf::RenderWindow& getWindow(){ return main;  }

	static AppStateType getCurrentState() { return stackofstates.back(); }
    static void changeState(AppStateType rump);
	static sf::Texture& getTexture(TextureIdentifier id);
    static sf::Font& getFont(FontIdentifier id);
    static void resauces();
	static void start();
	static void quit(void);
	static Game& getGame(){ return someGame;
 }
	static Splash& getTitle() {return Title;}
private:



	static AppStates* getState(AppStateType rump);
    static Textures textures;
    static Fonts fonts;
    static bool Loaded;
    static std::deque<AppStateType> stackofstates;
    static sf::RenderWindow main;
    static Game someGame;
    static GameOver someGameOver;
    static Splash Title;
    static Setup someSetup;
    static AppStates* currentState;
};

#endif // APPLY_HPP
