/* 
 * File:   WeakExplosion.hpp
 * Author: iivari
 *
 * Created on December 9, 2015, 11:27 AM
 */

#ifndef WEAKEXPLOSION_HPP
#define	WEAKEXPLOSION_HPP

#include "ExplosionType.hpp"

class WeakExplosion: public ExplosionType {
public:
    WeakExplosion();
    virtual int simulateExplosionDamage(sf::Vector2f, Stronghold, float) const;

    virtual void makeDamage(sf::Vector2f, std::vector<Stronghold>&, float) {
    }
private:

};

#endif	/* WEAKEXPLOSION_HPP */

