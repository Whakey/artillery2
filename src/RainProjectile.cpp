/* 
 * File:   RainProjectile.cpp
 * Author: iivari
 * 
 * Created on November 25, 2015, 7:21 PM
 */

#include "RainProjectile.hpp"
#include "BasicExplosion.hpp"
#include "ExplosionType.hpp"

RainProjectile::RainProjectile() : ProjectileItem(2000, 20, 60, true, 3, 40, 10000, std::make_shared<BasicExplosion>()) {
    std::vector<sf::Vector2f> dpv;
    dpv.push_back(sf::Vector2f(5, 0));
    dpv.push_back(sf::Vector2f(7, 0));
    dpv.push_back(sf::Vector2f(9, 0));
    dpv.push_back(sf::Vector2f(11, 0));
    dpv.push_back(sf::Vector2f(5, 1));
    dpv.push_back(sf::Vector2f(7, 1));
    dpv.push_back(sf::Vector2f(9, 1));
    dpv.push_back(sf::Vector2f(11, 1));
    dpv.push_back(sf::Vector2f(5, 2));
    dpv.push_back(sf::Vector2f(7, 2));
    dpv.push_back(sf::Vector2f(9, 2));
    dpv.push_back(sf::Vector2f(11, 2));
    dpv.push_back(sf::Vector2f(5, 3));
    dpv.push_back(sf::Vector2f(7, 3));
    dpv.push_back(sf::Vector2f(9, 3));
    dpv.push_back(sf::Vector2f(11, 3));
    dpv.push_back(sf::Vector2f(5, 4));
    dpv.push_back(sf::Vector2f(7, 4));
    dpv.push_back(sf::Vector2f(9, 4));
    dpv.push_back(sf::Vector2f(11, 4));

    std::vector<sf::Vector2f> dvv;
    
    dvv.push_back(sf::Vector2f(-35, -30));
    dvv.push_back(sf::Vector2f(-70, 30));
    dvv.push_back(sf::Vector2f(7, 23));
    dvv.push_back(sf::Vector2f(8, -4));
    dvv.push_back(sf::Vector2f(10, 3));
    dvv.push_back(sf::Vector2f(40, 34));
    dvv.push_back(sf::Vector2f(30, -6));
    dvv.push_back(sf::Vector2f(10, -8));
    dvv.push_back(sf::Vector2f(2, 3));
    dvv.push_back(sf::Vector2f(4, 20));
    dvv.push_back(sf::Vector2f(15, 23));
    dvv.push_back(sf::Vector2f(11, 12));
    dvv.push_back(sf::Vector2f(22, -5));
    dvv.push_back(sf::Vector2f(35, -23));
    dvv.push_back(sf::Vector2f(35, -23));
    dvv.push_back(sf::Vector2f(20, 12));
    dvv.push_back(sf::Vector2f(31, 43));
    dvv.push_back(sf::Vector2f(23, 17));
    dvv.push_back(sf::Vector2f(24, 12));
    dvv.push_back(sf::Vector2f(25, -12));

    setDisplacements(dpv);
    setDivergeVelocitys(dvv);
    
    setFillAndOutColor(sf::Color::Red, sf::Color::Black);

    setName("Rainprojectile");
    setSoundType(EXPLOSION3);
}
