/* 
 * File:   main.cpp
 * Author: iivari
 *
 * Created on November 10, 2015, 1:56 AM
 */

#include "../TrajectoryManager.hpp"
#include "../FlyingProjectiles.hpp"
#include "../Terrain.hpp"
#include "../Shop.hpp"
#include "../SoundList.hpp"
#include <iostream>
#include <random>
#include <memory>

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

int main() {
    //intialize everything

	//RNG for wind
	std::uniform_int_distribution<> randomWindGenerator(-20, 20);
	std::uniform_int_distribution<> randomWindAdder(-5, 5);
	std::mt19937 gen((unsigned int)time(NULL));
	float wind = (float)randomWindGenerator(gen);

    SoundList voicelist = SoundList();
    voicelist.initializeSounds();

    sf::Texture avatar;
    Player p1 = Player(avatar, "player1", false, 100);
    Player p2 = Player(avatar, "player2", false, 100);
    Shop shop;

    sf::RenderWindow window(sf::VideoMode(1000, 1000), "Artillery 2");

    sf::Vector2i size(1000, 1000);
    Terrain background(size);
    background.generateTerrain(10, 300);

	//Define the background sky
	sf::Image sky;
	sky.create(size.x, size.y, sf::Color(0, 191, 255, 255));
	sf::Texture skytxtr;
	skytxtr.loadFromImage(sky);
	sf::Sprite skysprite;
	skysprite.setTexture(skytxtr);

	//This is used as an indicator to show where you are aiming
	sf::CircleShape crosshair(10);
	crosshair.setFillColor(sf::Color::White);
	crosshair.setOutlineColor(sf::Color::Black);
	crosshair.setOutlineThickness(5);
	crosshair.setOrigin(5, 100);

	//Wind indicator
	sf::CircleShape windIndicator(10, 3);
	windIndicator.setOrigin(sf::Vector2f(5, 5));
	windIndicator.setFillColor(sf::Color::White);
	windIndicator.setOutlineColor(sf::Color::Black);
	windIndicator.setOutlineThickness(1);
	windIndicator.setRotation(90);
	windIndicator.setPosition(sf::Vector2f(size.x / 2, 20));

    FlyingProjectiles f_projectiles(size.y, voicelist);
    TrajectoryManager tr(wind, size.y, background);

    std::list<std::shared_ptr<Stronghold>> strongholds;
    strongholds.push_back(std::make_shared<Stronghold>(background.terrainHeight(sf::Vector2i(50, 50)), p1));
    strongholds.push_back(std::make_shared<Stronghold>(background.terrainHeight(sf::Vector2i(850, 50)), p2));


    auto current = strongholds.begin();
    sf::Clock clock;

    float power = 100;
    float angle = 3.14;
    std::string str = "Basic";
    //gameloop starts
    
    while (window.isOpen()) {
        sf::Event event;
	
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        if (f_projectiles.get_size()) {//tämä sen takia jotta voin päätellä uuden pelaajan iteraattorin loopin jälkeen
            while (f_projectiles.get_size() != 0 && window.isOpen()) { //kun ammus lentää niin tämä pyörii
                clock.restart();
                sf::Event event;
                while (window.pollEvent(event)) {
                    if (event.type == sf::Event::Closed)
                        window.close();
                }
                tr.UpdateProjectiles(f_projectiles);
                f_projectiles.deleteAndExplodeUselesProjectiles(strongholds, background, **current);
                window.clear();
                background.updateSprite();
				window.draw(skysprite);
                window.draw(background.getSprite());
                f_projectiles.draw(window, background);
                for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
                    //                std::cout << "Terrain height at point: y " << background.terrainHeight((*it)->getLocation()).y << " x: " << background.terrainHeight((*it)->getLocation()).x << " Base location: y " << (*it)->getLocation().y << " x: " << (*it)->getLocation().x << std::endl;
                    (*it)->update();
                    window.draw((*it)->getStrongholdSprite());
                }

				//Draw health bars
				for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
					sf::RectangleShape redbar(sf::Vector2f(50, 10));
					sf::RectangleShape greenbar(sf::Vector2f((*it)->getHealth() / 2, 10));
					redbar.setFillColor(sf::Color::Red);
					greenbar.setFillColor(sf::Color::Green);

					sf::Vector2i healthbarpos = (*it)->getLocation();
					redbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));
					greenbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));

					window.draw(redbar);
					window.draw(greenbar);


				}

				//Draw wind indicator
				windIndicator.setScale(sf::Vector2f(1, wind/2));
				window.draw(windIndicator);

                sf::Time time = clock.getElapsedTime();
                sf::Time sleeptime = sf::microseconds(17000) - time;
                sf::sleep(sleeptime);
                window.display();
                std::cout << "Amount of projectiles: " << f_projectiles.get_size() << " Time to refresh picture: " << (double) time.asMicroseconds() / 1000 << " ms and sleeping " << (double) sleeptime.asMicroseconds() / 1000 << " ms" << std::endl;
            }
             //tämän jälkeen hankitaan seuraavan pelaajan tiedot
             wind += (float)randomWindAdder(gen);
            if ((*current)->isDestroyed())
                break;
            current++;
            if (current == strongholds.end()) {
                current = strongholds.begin();
            }

            if ((*current)->isDestroyed())
                break;
        }
		//päivitetään ikkunaa
		sf::Vector2i crosshairpos = (*current)->getLocation();
		crosshair.setPosition(sf::Vector2f((float)crosshairpos.x, (float)crosshairpos.y - 20));
		crosshair.setRotation(angle*57.296 + 180); //radians to degrees
		crosshair.setOrigin(5, power + 40);

        window.clear();
        background.updateSprite();
		window.draw(skysprite);
        window.draw(background.getSprite());
		window.draw(crosshair); //Draw crosshair
        f_projectiles.draw(window, background);
        for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
            (*it)->update();
            window.draw((*it)->getStrongholdSprite());
        }

		//Draw health bars
		for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
			sf::RectangleShape redbar(sf::Vector2f(50, 10));
			sf::RectangleShape greenbar(sf::Vector2f((*it)->getHealth()/2, 10));
			redbar.setFillColor(sf::Color::Red);
			greenbar.setFillColor(sf::Color::Green);

			sf::Vector2i healthbarpos = (*it)->getLocation();
			redbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));
			greenbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));
			
			window.draw(redbar);
			window.draw(greenbar);
		}

		//Draw wind indicator
		windIndicator.setScale(sf::Vector2f(1, wind/2));
		window.draw(windIndicator);

        window.display();



	 //katsotaan pelaajan antamat syötteet
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            f_projectiles.addProjectile((*current)->getLocation(), tr.convertToVelocity(power, angle), shop.buyProjectileItem((*current)->getOwner(), str), 0);

            if (str == "Basic") voicelist.playSound(SHOOT3);
            else if (str == "Rain") voicelist.playSound(SHOOT4);
            else if (str == "Divide") voicelist.playSound(SHOOT7);
            else if (str == "Dirt Eater") voicelist.playSound(SHOOT6);
            power = 100;
            angle = 3.14;
            str = "Basic";
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            if (power < 200) {
                power += 1;
                voicelist.playSound(CLICK);
            } else voicelist.playSound(ERROR);
            std::cout << "power" << power << std::endl;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
            if (power > 10) {
                power -= 1;
                voicelist.playSound(CLICK);
            } else voicelist.playSound(ERROR);
            std::cout << "power" << power << std::endl;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            if (angle < 2 * 3.14) {
                angle += 0.01;
                voicelist.playSound(CLICK);
            } else voicelist.playSound(ERROR);
            std::cout << "angle" << angle << std::endl;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            if (angle > 0) {
                voicelist.playSound(CLICK);
                angle -= 0.01;
            } else voicelist.playSound(ERROR);
            std::cout << "angle" << angle << std::endl;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::B)) {
            voicelist.playSound(PURCHASE);
	    sf::sleep(sf::seconds(1));
            str = "Basic";
        }
       if (sf::Keyboard::isKeyPressed(sf::Keyboard::R)) {
            voicelist.playSound(PURCHASE);
            sf::sleep(sf::seconds(1));
	    str = "Rain";
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            voicelist.playSound(PURCHASE);
            sf::sleep(sf::seconds(1));
	    str = "Divide";
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
            voicelist.playSound(PURCHASE);
            sf::sleep(sf::seconds(1));
	    str = "Dirt Eater";
        }
    }
    
    //lopuksi tulokset terminaaliin
    auto it = strongholds.begin();
    std::cout << "RESULTS:" << std::endl << (*it)->getOwner().getName() << " health: " << (*it)->getHealth() << " damage " << (*it)->getOwner().getCausedHavoc() << " money " << (*it)->getOwner().getMoney() << " kills " << (*it)->getOwner().getKills() << " deaths " << (*it)->getOwner().getDeaths() << std::endl;
    it++;
    std::cout << (*it)->getOwner().getName() << " health: " << (*it)->getHealth() << " damage " << (*it)->getOwner().getCausedHavoc() << " money " << (*it)->getOwner().getMoney() << " kills " << (*it)->getOwner().getKills() << " deaths " << (*it)->getOwner().getDeaths() << std::endl;

    
    voicelist.playSound(WINNING);
    strongholds.clear();
    sf::sleep(sf::seconds(5));
    return 0;
}

