/* 
 * File:   BasicProjectile.hpp
 * Author: iivari
 *
 * Created on November 10, 2015, 3:35 AM
 */

#ifndef BASICPROJECTILE_HPP
#define	BASICPROJECTILE_HPP

#include "ProjectileItem.hpp"
#include "FlyingProjectiles.hpp"

class Projectile;

class BasicProjectile : public ProjectileItem {
public:
    BasicProjectile();

    virtual bool canMakeToProjectiles(const sf::Vector2f location, const sf::Vector2f velocity, int divide_count) const{
        return false;
    }


};

#endif	/* BASICPROJECTILE_HPP */

