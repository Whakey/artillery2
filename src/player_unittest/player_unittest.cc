#include <limits.h>
#include "../Player.hpp"
#include "gtest/gtest.h"


TEST(Player, add){
  sf::Texture texture;
  Player *player1 = new Player(texture, "Iivari", false, 100);
  EXPECT_EQ("Iivari",player1->getName());
  EXPECT_EQ(100 ,player1->getMoney());
}
TEST(Player, makeMoney){
  sf::Texture texture;
  Player player(texture, "Iivari", false, 100);
  player.changeMoneyAmount(30);
   EXPECT_EQ(130 ,player.getMoney());
}

TEST(Player, die){
  sf::Texture texture;
  Player player(texture, "Iivari", false, 100);
  player.addDeath();
   EXPECT_EQ(1 ,player.getDeaths());
}

/*
int getMoney() const {
        return money;
    }

    unsigned int getKills() const {
        return kills;
    }

    unsigned int getDeaths() const {
        return deaths;
    }

    unsigned int getCausedHavoc() const {
        return caused_havoc;
    }

    bool getIfAi() const {
        return is_ai;
    }

    std::string getName() {
        return name;
    }

    sf::Texture getAvatar() {
        return avatar;
    }

    void addHavoc(unsigned int havoc) {
        caused_havoc += havoc;
    }

    void addKill() {
        kills += 1;
    }

    void addDeath() {
        deaths += 1;
    }
    bool changeMoneyAmount(int);
*/