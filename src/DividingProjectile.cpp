/* 
 * File:   DividingProjectile.cpp
 * Author: iivari
 * 
 * Created on November 18, 2015, 1:42 PM
 */

#include "DividingProjectile.hpp"
#include "BasicExplosion.hpp"
#include "ExplosionType.hpp"

DividingProjectile::DividingProjectile() : ProjectileItem(1, 4, 20, true, 3, 40, 8000, std::make_shared<BasicExplosion>()) {
    std::vector<sf::Vector2f> dpv;
    dpv.push_back(sf::Vector2f(5,0));
    dpv.push_back(sf::Vector2f(7,0));
    dpv.push_back(sf::Vector2f(9,0));
    dpv.push_back(sf::Vector2f(11,0));
    
    std::vector<sf::Vector2f> dvv;
    
    dvv.push_back(sf::Vector2f(5,40));
    dvv.push_back(sf::Vector2f(7,2));
    dvv.push_back(sf::Vector2f(8,-5));
    dvv.push_back(sf::Vector2f(10,0));
    
    setDisplacements(dpv);
    setDivergeVelocitys(dvv);
    
    setFillAndOutColor(sf::Color::Blue, sf::Color::Black);
    
    setName("Dividing projectile");
    setSoundType(EXPLOSION2);
}



