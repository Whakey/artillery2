/* 
 * File:   TrajectoryManager.hpp
 * Author: iivari
 *
 * Created on November 10, 2015, 12:13 AM
 */

#ifndef TRAJECTORYMANAGER_HPP
#define	TRAJECTORYMANAGER_HPP

#include "Projectile.hpp"
#include "FlyingProjectiles.hpp"
#include "Terrain.hpp"

class TrajectoryManager {
public:

    TrajectoryManager(const float & wind, int height, Terrain & terrain) : wind(wind), height(height), terrain(terrain) {
    }
    void UpdateProjectiles(FlyingProjectiles&);
    void ChangeNextVelocityLocation(Projectile&);
    sf::Vector2f convertToVelocity(float power, float angle);
    //FlyingProjectiles getResult(Stronghold, float power, float angle, const ProjectileItem& type);
    
private:
    float SIMULTATIONSPEED = 3; // making this value too high will cause penetration in terrain
    float timestep = 0.016667 * SIMULTATIONSPEED;
    const float &wind;
    int height;
    Terrain& terrain;
    std::clock_t last_end;

};

#endif	/* TRAJECTORYMANAGER_HPP */

