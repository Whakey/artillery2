/* 
 * File:   SoundList.cpp
 * Author: hopsale
 * 
 * Created on November 12, 2015, 9:36 PM
 */

#include "SoundList.hpp"

SoundList::SoundList() { size = 0; }

void SoundList::addSound(std::shared_ptr<Sound> sound) {
    sounds.push_back(sound);
    size += 1;
    
}

std::vector<std::shared_ptr<Sound>>::iterator SoundList::findSound(SoundType type) {
    for (it = sounds.begin(); it < sounds.end(); it++) {
        if ((*it)->getType() == type) {
            return it;
        }
    }
    return it;
}

void SoundList::initializeSounds() {
  
    addSound(std::make_shared<Sound>(getMainPath() + "click.wav", CLICK));
    addSound(std::make_shared<Sound>(getMainPath() + "error.wav", ERROR));
    addSound(std::make_shared<Sound>(getMainPath() + "explosionbig.wav", EXPLOSIONBIG));
    addSound(std::make_shared<Sound>(getMainPath() + "explosionsmall.wav", EXPLOSIONSMALL));
    addSound(std::make_shared<Sound>(getMainPath() + "explosion.wav", EXPLOSION));
    addSound(std::make_shared<Sound>(getMainPath() + "explosion2.wav", EXPLOSION2));
    addSound(std::make_shared<Sound>(getMainPath() + "explosion3.wav", EXPLOSION3));
    addSound(std::make_shared<Sound>(getMainPath() + "explosion4.wav", EXPLOSION4));
    addSound(std::make_shared<Sound>(getMainPath() + "explosion5.flac", EXPLOSION5));
    addSound(std::make_shared<Sound>(getMainPath() + "purchase.wav", PURCHASE));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot.flac", SHOOT));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot2.wav", SHOOT2));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot3.ogg", SHOOT3));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot4.wav", SHOOT4));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot5.wav", SHOOT5));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot6.wav", SHOOT6));
    addSound(std::make_shared<Sound>(getMainPath() + "shoot7.wav", SHOOT7));
    addSound(std::make_shared<Sound>(getMainPath() + "toilet.wav", RANDOM));
    addSound(std::make_shared<Sound>(getMainPath() + "kanuunalierot3000.wav", START));
    addSound(std::make_shared<Sound>(getMainPath() + "winning.wav", WINNING));
    
    //std::shared_ptr<Sound> clicketyclick = std::make_shared<Sound>("Sounds/explosion.wav", CLICK);
    //addSound(clicketyclick);
    
}

void SoundList::playSound(SoundType type) {
  it = findSound(type);
  if (it != sounds.end()) 
      (*it)->play();
  else 
      std::cout << "Error finding SoundType #" << std::to_string(type) << " from list of sounds" << std::endl;
}

void SoundList::playAllSounds() {
    for (it = sounds.begin(); it < sounds.end(); it++) {
        (*it)->play();
        
        while((*it)->getStatus() == sf::SoundSource::Playing) {
            continue;
        }   
    }
}

int SoundList::getLength() {
    return size;
}

//void SoundList::removeSound(SoundType type) {
//    it = findSound(type);
//    if (it != sounds.end()) {
//        sounds.erase(it);
//        size -= 1;
//    }
//}

//void SoundList::setSounds(std::vector<std::pair<std::string, SoundType>> listOfSounds) {
//    for (it2 = listOfSounds.begin(); it2 < listOfSounds.end(); it2++) {
//        std::shared_ptr<Sound> onesound = std::make_shared<Sound>(it2->first, it2->second);
//        addSound(onesound);
//    }
//}

