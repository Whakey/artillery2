#ifndef GAME_H
#define	GAME_H
 // we will utilize this once only
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"

// implement other class imports here (.h/hpps)


class Game
{

public:
	Game();
	~Game();
  static void Start();
	

private:
  static bool Exits();
  static void GameLoop();
  
static void ShowSplash();
static void ShowMenu();
  enum GameState { splash, uninit,
          ShowingMenu, Playing, Exiting };
  
  static GameState _gamest;
  static sf::RenderWindow _main;

// base class here as a static
};

#endif
