
#include <stdio.h>

// additional headers
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

#include <map>
#include <iostream>
#include <cassert>


#include "TrajectoryManager.hpp"
#include "FlyingProjectiles.hpp"
#include "Terrain.hpp"
#include "Shop.hpp"
#include "SoundList.hpp"
#include "gamedemo.h"
#include "mainmenudemo.h"
#include "splashdemo.h"

//some imports, especially sound here

Game::Game(){}

void Game::Start(void) {

if (_gamest != uninit)
return;

    _main.create(sf::VideoMode(800, 600, 32),"Kanuunalierot");


    

_gamest = Game::splash;
    while (!Exits()) {
        GameLoop(); // jattaa pelin auki niin kauan kunnes ikkuna suljetaan
    }

    _main.close();
}

bool Game::Exits() {
    if (_gamest == Game::Exiting)
        return true;
    else
        return false;
}


// possible object manager here (as a const return)

void Game::GameLoop() {


   //intialize everything

	//RNG for wind
	std::uniform_int_distribution<> randomWindGenerator(-10, 10);
	std::mt19937 gen((unsigned int)time(NULL));
	float wind = (float)randomWindGenerator(gen);

    SoundList voicelist = SoundList();
    voicelist.initializeSounds();

    sf::Texture avatar;
    Player p1 = Player(avatar, "player1", false, 100);
    Player p2 = Player(avatar, "player2", false, 100);
    Shop shop;

    sf::Vector2i size(800, 600);
    Terrain background(size);
    background.generateTerrain(10, 300);

	//Define the background sky
	sf::Image sky;
	sky.create(size.x, size.y, sf::Color(0, 191, 255, 255));
	sf::Texture skytxtr;
	skytxtr.loadFromImage(sky);
	sf::Sprite skysprite;
	skysprite.setTexture(skytxtr);

	//This is used as an indicator to show where you are aiming
	sf::CircleShape crosshair(10);
	crosshair.setFillColor(sf::Color::White);
	crosshair.setOutlineColor(sf::Color::Black);
	crosshair.setOutlineThickness(5);
	crosshair.setOrigin(5, 100);

	//Wind indicator
	sf::CircleShape windIndicator(10, 3);
	windIndicator.setFillColor(sf::Color::White);
	windIndicator.setOutlineColor(sf::Color::Black);
	windIndicator.setOutlineThickness(1);
	windIndicator.setRotation(90);
	windIndicator.setPosition(sf::Vector2f(size.x / 2, 20));

    FlyingProjectiles f_projectiles(size.y, voicelist);
    TrajectoryManager tr(wind, size.y, background);

    std::list<std::shared_ptr<Stronghold>> strongholds;
    strongholds.push_back(std::make_shared<Stronghold>(background.terrainHeight(sf::Vector2i(50, 50)), p1));
    strongholds.push_back(std::make_shared<Stronghold>(background.terrainHeight(sf::Vector2i(850, 50)), p2));


    auto current = strongholds.begin();
    sf::Clock clock;

    float power = 100;
    float angle = 3.14;
    std::string str = "Basic";

    sf::Event event;
    _main.pollEvent(event);

        switch (_gamest) {
            
            case Game::splash:{
                ShowSplash();
                break;
            }
            
            case Game::ShowingMenu:
            {
                ShowMenu();
                break;
            }
            
            
            case Game::Playing:
            {
                _main.clear(sf::Color(0, 0, 0)); // alustava, voidaan muuttaa varia

		// Terrain update, and object update, i.e. enviro initialization here


                _main.display();
                
            if (event.type == sf::Event::Closed)
                _main.close();
        
        if (f_projectiles.get_size()) {//t�m� sen takia jotta voin p��tell� uuden pelaajan iteraattorin loopin j�lkeen
            while (f_projectiles.get_size() != 0 && _main.isOpen()) { //kun ammus lent�� niin t�m� py�rii
                clock.restart();
                sf::Event event;
                if (event.type == sf::Event::Closed)
                        _main.close();

                tr.UpdateProjectiles(f_projectiles);
                f_projectiles.deleteAndExplodeUselesProjectiles(strongholds, background, **current);
                _main.clear();
                background.updateSprite();
				_main.draw(skysprite);
                _main.draw(background.getSprite());
                f_projectiles.draw(_main, background);
                for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
                    //                std::cout << "Terrain height at point: y " << background.terrainHeight((*it)->getLocation()).y << " x: " << background.terrainHeight((*it)->getLocation()).x << " Base location: y " << (*it)->getLocation().y << " x: " << (*it)->getLocation().x << std::endl;
                    (*it)->update();
                    _main.draw((*it)->getStrongholdSprite());
                }

				//Draw health bars
				for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
					sf::RectangleShape redbar(sf::Vector2f(50, 10));
					sf::RectangleShape greenbar(sf::Vector2f((*it)->getHealth() / 2, 10));
					redbar.setFillColor(sf::Color::Red);
					greenbar.setFillColor(sf::Color::Green);

					sf::Vector2i healthbarpos = (*it)->getLocation();
					redbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));
					greenbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));

					_main.draw(redbar);
					_main.draw(greenbar);


				}

				//Draw wind indicator
				windIndicator.setScale(sf::Vector2f(1, wind));
				_main.draw(windIndicator);

                sf::Time time = clock.getElapsedTime();
                sf::Time sleeptime = sf::microseconds(17000) - time;
                sf::sleep(sleeptime);
                _main.display();
                std::cout << "Amount of projectiles: " << f_projectiles.get_size() << " Time to refresh picture: " << (double) time.asMicroseconds() / 1000 << " ms and sleeping " << (double) sleeptime.asMicroseconds() / 1000 << " ms" << std::endl;
            }
             //t�m�n j�lkeen hankitaan seuraavan pelaajan tiedot
            if ((*current)->isDestroyed())
                break;
            current++;
            if (current == strongholds.end()) {
                current = strongholds.begin();
            }

            if ((*current)->isDestroyed())
                break;
        }
		//p�ivitet��n ikkunaa
		sf::Vector2i crosshairpos = (*current)->getLocation();
		crosshair.setPosition(sf::Vector2f((float)crosshairpos.x, (float)crosshairpos.y - 20));
		crosshair.setRotation(angle*57.296 + 180); //radians to degrees
		crosshair.setOrigin(5, power + 40);

        _main.clear();
        background.updateSprite();
		_main.draw(skysprite);
        _main.draw(background.getSprite());
		_main.draw(crosshair); //Draw crosshair
        f_projectiles.draw(_main, background);
        for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
            (*it)->update();
            _main.draw((*it)->getStrongholdSprite());
        }

		//Draw health bars
		for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
			sf::RectangleShape redbar(sf::Vector2f(50, 10));
			sf::RectangleShape greenbar(sf::Vector2f((*it)->getHealth()/2, 10));
			redbar.setFillColor(sf::Color::Red);
			greenbar.setFillColor(sf::Color::Green);

			sf::Vector2i healthbarpos = (*it)->getLocation();
			redbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));
			greenbar.setPosition(sf::Vector2f(healthbarpos.x - 25, healthbarpos.y - 65));
			
			_main.draw(redbar);
			_main.draw(greenbar);
		}

		//Draw wind indicator
		windIndicator.setScale(sf::Vector2f(1, wind));
		_main.draw(windIndicator);

        _main.display();



	 //katsotaan pelaajan antamat sy�tteet
        if (event.key.code == sf::Keyboard::Space) {
            f_projectiles.addProjectile((*current)->getLocation(), tr.convertToVelocity(power, angle), shop.buyProjectileItem((*current)->getOwner(), str), 0);
            
	    if (str == "Basic") voicelist.playSound(SHOOT3);
	    else if (str == "Rain") voicelist.playSound(SHOOT4);
	    else if (str == "Divide") voicelist.playSound(SHOOT7);
	    else if (str == "Dirt Eater") voicelist.playSound(SHOOT6);
	    power = 100;
            angle = 3.14;
            str = "Basic";
        }
        if (event.key.code == sf::Keyboard::Up) {
            if (power < 200) {
                power += 5;
		voicelist.playSound(CLICK);
	    }
	    else voicelist.playSound(ERROR);
            std::cout << "power" << power << std::endl;
        }
        if (event.key.code == sf::Keyboard::Down) {
	  if (power > 10) {
                power -= 5;
		voicelist.playSound(CLICK);
	  }
	  else voicelist.playSound(ERROR);
            std::cout << "power" << power << std::endl;
        }

        if (event.key.code == sf::Keyboard::Right) {
            if (angle < 2 * 3.14) {
                angle += 0.1;
		voicelist.playSound(CLICK);
	    }
	    else voicelist.playSound(ERROR);
            std::cout << "angle" << angle << std::endl;
        }
        if (event.key.code == sf::Keyboard::Left) {
            if (angle > 0) {
	        voicelist.playSound(CLICK);
                angle -= 0.1;
	    }
	    else voicelist.playSound(ERROR);
            std::cout << "angle" << angle << std::endl;
        }
        if (event.key.code == sf::Keyboard::B) {
	  voicelist.playSound(PURCHASE);
            str = "Basic";
        }
        if (event.key.code == sf::Keyboard::R) {
	  voicelist.playSound(PURCHASE);
            str = "Rain";
        }
        if (event.key.code == sf::Keyboard::D) {
	  voicelist.playSound(PURCHASE);
            str = "Divide";
        }
        if (event.key.code == sf::Keyboard::E) {
	  voicelist.playSound(PURCHASE);
            str = "Dirt Eater";
        }

        event.key.code = sf::Keyboard::A; //laitetaan event keyksi A jotta vanha kirjain poistuu muistista (aika brutea) :D
                

                break;
            }
        }
}




void Game::ShowSplash()
{
	Splash splash;
	splash.Show(_main);
	_gamest = Game::ShowingMenu;
}

void Game::ShowMenu()
{
	MainMenu menu;
	MainMenu::Result res = menu.Show(_main);
	switch(res)
	{
	case MainMenu::Exit:
			_gamest = Game::Exiting;
			break;
	case MainMenu::Play:
			_gamest = Game::Playing;
			break;
	}
}




// alustus


Game::GameState Game::_gamest = uninit;
sf::RenderWindow Game::_main;
