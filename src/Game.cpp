#include "Apply.hpp"
#include "HUD.hpp"
#include "TrajectoryManager.hpp"
#include "FlyingProjectiles.hpp"
#include "BasicProjectile.hpp"
#include "Terrain.hpp"
#include "Shop.hpp"
#include "SoundList.hpp"
#include <list>
#include <string>
#include <cassert>

Game::Game() :
    AppStates(GameState),
	Curplayerint(0),
	wind(0)
{

	sf::Vector2i size(800, 600); //Size of the main game window (terrain uses this), can be changed 
	
	//Randomly generate terrain
	background = Terrain(size); 
	background.generateTerrain(10, 300);
	healthplayer(Curplayerint, strongholds[0]);
	voicelist.initializeSounds();
	f_projectiles(size.y);
    tr(wind, size.y, background);
	power = 100;
    angle = 3.14;
	}
void Game::renew()
{
    newGame();
}


void Game::draw(sf::RenderWindow &window)
{
	background.updateSprite();
	window.draw(background.getSprite());
	healthplayer.draw(window);
	sf::CircleShape crosshair(10);
	crosshair.setFillColor(sf::Color::White);
	crosshair.setOrigin(5, 50)


}
void Game::update(float deltat)
{
	int count = 0;
	for (auto pp : strongholds){
		healthplayer.stepper(*pp); //stronghold updates
    if((*pp).isDestroyed()) //scans for destroyed strongholds
        {
            count++;
        }}
	if (count == players.size()){ //int vs size_type?
		Apply:changeState(GameOverState);
		renew();}


}
void Game::pass(sf::Event Event)
{
    if(Event.type == sf::Event::KeyPressed)
    {

	if (event.key.code == sf::Keyboard::Return) {
		if (IsPlayerTurn(strongholds[Curplayerint])){
            f_projectiles.addProjectile(strongholds[Curplayerint].getLocation(), tr.convertToVelocity(power, angle), shop.buyProjectileItem(strongholds[Curplayerint].getOwner(), str), 0);
	}}
	if (event.key.code == sf::Keyboard::Up) {
		if (IsPlayerTurn(strongholds[Curplayerint])){
                     if (power < 200) {
                power += 5;
				voicelist.playSound(CLICK);}

	}}
	if (event.key.code == sf::Keyboard::Down) {
		if (IsPlayerTurn(strongholds[Curplayerint])){
            if (power > 10) {
                power -= 5;
		voicelist.playSound(CLICK);
	  }
	}}
	if (event.key.code == sf::Keyboard::Right) {
		if (IsPlayerTurn(strongholds[Curplayerint])){
     if (angle < 2 * 3.14) {
                angle += 0.1;
		voicelist.playSound(CLICK);
	    }
	}}
	if (event.key.code == sf::Keyboard::Left) {
		if (IsPlayerTurn(strongholds[Curplayerint])){
            if (angle > 0) {
	        voicelist.playSound(CLICK);
                angle -= 0.1;
	    }
	}}

	
	}


}

void Game::newGame(int player_count) {

    sf::Vector2i size(800, 600);
    terrainbackground.generateTerrain(10, 300);
    players.clear();
    strongholds.clear();
    Curplayerint = 0;
    int pw = size.x / player_count;
    for (int i = 0; i < player_count; ++i) {
        sf::Texture avatar;
        str = "player" + std::to_string(i);
        std::shared_ptr<Player> player= std::make_shared<Player>(avatar, str, false, 100);
        players.push_back(player);
        int x = rand() % (pw - 50) + pw * i + 25;
        strongholds.push_back(Stronghold(background.terrainHeight(sf::Vector2i(x, 0)), *player));
    }
}

bool Game::IsPlayerTurn(Stronghold current) {

	int flag = 0;
	   if (current == strongholds.end()) {
           current = strongholds.begin();
		   flag = 1;
            }
	    if (current.isDestroyed()){
			Curplayerint++;
			if (flag == 1) {
            Curplayerint = 0;;
            }
			return false;}
        if (f_projectiles.get_size()) {
            while (f_projectiles.get_size() != 0)) {
             
                tr.UpdateProjectiles(f_projectiles);
                f_projectiles.draw(window, background);
                for (auto it = strongholds.begin(); it != strongholds.end(); it++) {
					(*it)->update();
                    window.draw((*it)->getStrongholdSprite());
                }

            if (flag == 1) {
                Curplayerint = 0;
            }
			else {
				Curplayerint++;}

			}
		}
		return true;
}
