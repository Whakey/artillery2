#ifndef APPSTATES_HPP
#define APPSTATES_HPP
// done by Matvej

#include <memory>
#include <deque>

#include <SFML/Graphics.hpp>

enum AppStateType
{
    SplashState,
    SetupState,
    GameState,
    GameOverState
};

class AppStates
{
    public:
        AppStates();
        AppStates(AppStateType t) : type(t) {};
        virtual ~AppStates(){};
		virtual void update(float deltat) = 0;
        virtual void pass(sf::Event event) = 0;
        virtual void renew() = 0;
        virtual void draw(sf::RenderWindow &window) = 0;
		// we have to nullify these for the sake of implementing them later @valikko/splash menu classes
        AppStateType type;
};

#endif // APPSTATES_HPP
