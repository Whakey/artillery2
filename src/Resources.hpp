#ifndef RESOURCES_HPP
#define RESOURCES_HPP
#include <memory>
#include <map>
#include <cassert>
// done by Matvej
template <typename Res, typename ID>
class Resources
{
    public:
        Resources();
        bool load(ID id, const std::string& filename)
        {
            std::unique_ptr<Res> neo(new Res());
            bool situation = neo->loadFromFile(filename);
            auto x = RMap.insert(std::make_pair(id,std::move(neo)));
            assert(x.second);
            return situation;
        }
		Res &get(ID id)
        {
            auto resus = RMap.find(id);
            assert(resus != RMap.end());
            return *(resus->second);
        }
        template<typename Parameter>
        bool load(ID id, const std::string& filename,Parameter Two)
        {
            std::unique_ptr<Res> neo(new Res());
            bool situation = neo->loadFromFile(filename,Two);
            RMap.insert(std::make_pair(id,std::move(neo)));
            return situation;
        }

    private:
        std::map<ID, std::unique_ptr<Res> > RMap;
};

// crucial header for upping resources and so on
#include "Resourcesids.hpp"
#endif // RESOURCES_HPP
