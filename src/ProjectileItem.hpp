/* 
 * File:   ProjectileItem.hpp
 * Author: iivari
 *
 * Created on November 9, 2015, 10:33 PM
 */

#ifndef PROJECTILEITEM_HPP
#define	PROJECTILEITEM_HPP

#include "SoundList.hpp"
#include <SFML/Graphics.hpp>
#include "ExplosionType.hpp"
#include <memory>

class Projectile;

class ProjectileItem {
public:

    ProjectileItem(int max_divide_count, int max_new_projectiles_in_divide, int price, bool ai_is_able_to_buy, float radius, float explosion_radius, float mass, std::shared_ptr<ExplosionType> explosion_type) :
    max_divide_count(max_divide_count), price(price), ai_is_able_to_buy(ai_is_able_to_buy), radius(radius), explosion_radius(explosion_radius), mass(mass), explosion_type(explosion_type), max_new_projectiles_in_divide(max_new_projectiles_in_divide){
    }
    
    void setSoundType(SoundType type) {
        sound = type;
    }
    
    SoundType getSoundType() const{
        return sound;
    }
    
    
    int getPrice() const {
        return price;
    }

    float getRadius() const {
        return radius;
    }

    float getExplosionRadius() const {
        return explosion_radius;
    }

    float getMass() const {
        return mass;
    }

    void setName(std::string n){
        type_name = n;
    }

    std::string getName() const{
        return type_name;
    }


    std::shared_ptr<ExplosionType> getExplosionType() const {
        return explosion_type;
    }

    int getMaxDivideCount() const {
        return max_divide_count;
    }

    void setDivergeVelocitys(std::vector<sf::Vector2f> dv) {
        for (auto it : dv) {
            diverge_velocitys.push_back(it);
        }
    }

    void setDisplacements(std::vector<sf::Vector2f> d) {
        for (auto it : d) {
            displacements.push_back(it);
        }
    }

    sf::Vector2f getDivergeVelocity(unsigned int i) const {
        if (diverge_velocitys.size() > i)
            return diverge_velocitys[i];
        return sf::Vector2f(0,0);
    }

    sf::Vector2f getDisplacement(unsigned int i) const {
        if (displacements.size() >  i)
            return displacements[i];
        return sf::Vector2f(0, 0);
    }

    int getMaxNewProjectilesInDivide() const {
        return max_new_projectiles_in_divide;
    }
    
    void setFillAndOutColor(sf::Color fill, sf::Color outline){
        fill_color = fill;
        outline_color = outline;
    }
    
    sf::Color getOutColor() const{
        return outline_color;
    }
    
    sf::Color getFillColor() const{
        return fill_color;
    }


    virtual bool canMakeToProjectiles(const sf::Vector2f location, const sf::Vector2f velocity, int divide_count) const = 0;

private:
    SoundType sound;
    std::string type_name;
    int max_divide_count;
    int price;
    bool ai_is_able_to_buy;
    float radius;
    float explosion_radius;
    float mass;
    std::shared_ptr<ExplosionType> explosion_type;
    int max_new_projectiles_in_divide;
    std::vector<sf::Vector2f> diverge_velocitys;
    std::vector<sf::Vector2f> displacements;
    sf::Color fill_color;
    sf::Color outline_color;
    

};

#endif	/* PROJECTILEITEM_HPP */

