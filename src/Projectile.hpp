/* 
 * File:   Projectile.hpp
 * Author: iivari
 *
 * Created on November 9, 2015, 11:22 PM
 */

#ifndef PROJECTILE_HPP
#define	PROJECTILE_HPP

#include <SFML/System.hpp>
#include "ProjectileItem.hpp"
#include "Terrain.hpp"
#include <iostream>
#include "SoundList.hpp"
#include <list>


class Projectile {
public:

    Projectile(sf::Vector2f position, sf::Vector2f velocity, const ProjectileItem& type, int divide_count, SoundList& voicelist) : 
    position(position), velocity(velocity), type(type), divide_count(divide_count), voicelist(voicelist) {
    }

    sf::Vector2f getPosition() const {
        return position;
    }

    sf::Vector2f getVelocity() const {
        return velocity;
    }

    const ProjectileItem& getType() const {
        return type;
    }

    sf::Vector2i getPositionInt() {
        return sf::Vector2i(position.x, position.y);
    }

    Projectile* clone() {
        return new Projectile(*this);
    }

    void changeVelocity(sf::Vector2f v2) {
        velocity += v2;
    }

    void changePosition(sf::Vector2f p2) {
        position += p2;
    }

    int getDivideCount() {
        return divide_count;
    }

    bool isExplosion() {
        return is_explosion;
    }

    void addDivide() {
        divide_count += 1;
    }

    bool isInGroundOrOut(Terrain &terrain);
    bool isUseles(Terrain& terrain);
    void makeToCircle();
    void draw(sf::RenderWindow &window, Terrain &terrain);
    int explodeProjectile(std::list<std::shared_ptr<Stronghold>>, Terrain& terrain, Stronghold&);


private:
    sf::Vector2f position;
    sf::Vector2f velocity;
    const ProjectileItem& type;
    sf::CircleShape representing_circle;
    int divide_count;
    int frames_in_ground = 0;
    bool is_explosion = false;
    bool has_been_drawed = false;
    SoundList& voicelist;
    

};



#endif	/* PROJECTILE_HPP */

