/* 
 * File:   DividingProjectile.hpp
 * Author: iivari
 *
 * Created on November 18, 2015, 1:42 PM
 */

#ifndef DIVIDINGPROJECTILE_HPP
#define	DIVIDINGPROJECTILE_HPP
#include "ProjectileItem.hpp"
#include "BasicExplosion.hpp"
#include "ExplosionType.hpp"

class Projectile;

class DividingProjectile : public ProjectileItem {
public:
    DividingProjectile();

    virtual bool canMakeToProjectiles(const sf::Vector2f location, const sf::Vector2f velocity, int divide_count) const {
        //return true;
        return (getMaxDivideCount() > divide_count) && velocity.y > 0;
    }

private:

};

#endif	/* DIVIDINGPROJECTILE_HPP */

