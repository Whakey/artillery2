#ifndef HUD_HPP
#define HUD_HPP
#include "Game.hpp"


class HUD
{
public:
    HUD(int getPlayerIndex, int healthbar);
    void draw(sf::RenderWindow &win);
    void stepper(Stronghold playersindx);
private:
    sf::Text health;
    sf::Text player;

};

#endif // HUD_HPP
