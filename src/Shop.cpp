/* 
 * File:   Shop.cpp
 * Author: iivari
 * 
 * Created on November 18, 2015, 6:54 PM
 */

#include "Shop.hpp"
#include "BasicProjectile.hpp"
#include "DividingProjectile.hpp"
#include "RainProjectile.hpp"
#include "DirtEaterProjectile.hpp"

Shop::Shop() : money_used(0) {
    add("Basic", std::make_shared<BasicProjectile>());
    add("Divide", std::make_shared<DividingProjectile>());
    add("Rain", std::make_shared<RainProjectile>());
    add("Dirt Eater", std::make_shared<DirtEaterProjectile>());
}

Shop::Shop(const Shop& orig) {
    while (!types.empty()) {
        types.erase(types.begin());
    }
    for (auto it : orig.types) {
        auto pair = std::make_pair(it.first, it.second);
        types.insert(pair);
    }
}

Shop::~Shop() {
  
}

void Shop::add(const std::string& str, std::shared_ptr<ProjectileItem> pi) {
    if (pi == NULL) {
        std::invalid_argument("Invalid Item");
    }
    for (auto it : types) {
        if (it.first == str) {
            throw std::invalid_argument("Duplicate index");
        }
    }
    types.insert(std::make_pair(str, pi));
}

ProjectileItem& Shop::operator[](const std::string& str) {
    for (auto it = types.begin(); it != types.end(); it++) {
        if (it->first == str)
            return *(it->second);
    }
    throw std::invalid_argument("Unknown index");
}

ProjectileItem& Shop::buyProjectileItem(Player& p, const std::string& str) {
    if (canPlayerBuy(p,str)){
      for (auto it = types.begin(); it != types.end(); it++) {
	  if (it->first == str) {
	      p.changeMoneyAmount(-(it->second->getPrice()));
	      money_used += it->second->getPrice();
	      return *(it->second);
	  }
      }
      
    }
    return *(types.begin()->second);
}

bool Shop::canPlayerBuy(const Player& p, const std::string& str) {
    for (auto it = types.begin(); it != types.end(); it++) {
        if (it->first == str)
            if (it->second->getPrice() <= p.getMoney())
                return true;
    }
    return false;
}

void Shop::buyMoreHealth(Stronghold& s, int amount) {

    int temp = s.getMaxthealth() - (s.getHealth() + amount);
    if (temp < 0) {
        money_used += amount + temp;
        s.getOwner().changeMoneyAmount(-amount - temp);
    }
    money_used += amount;
    s.heathChange(amount);

}
