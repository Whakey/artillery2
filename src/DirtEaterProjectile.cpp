/* 
 * File:   DirtEaterProjectile.cpp
 * Author: iivari
 * 
 * Created on December 9, 2015, 11:37 AM
 */

#include "DirtEaterProjectile.hpp"
#include "WeakExplosion.hpp"



DirtEaterProjectile::DirtEaterProjectile() : ProjectileItem(0, 0, 0, true, 3, 200, 5000, std::make_shared<WeakExplosion>()){
 
    setName("Dirt Eater projectile");
    setFillAndOutColor(sf::Color::Black, sf::Color::Green);
    setSoundType(EXPLOSIONBIG);
}

