/* 
 * File:   DirtEaterProjectile.hpp
 * Author: iivari
 *
 * Created on December 9, 2015, 11:37 AM
 */

#ifndef DIRTEATERPROJECTILE_HPP
#define	DIRTEATERPROJECTILE_HPP

#include "ProjectileItem.hpp"

class DirtEaterProjectile : public ProjectileItem{
public:
    DirtEaterProjectile();
     virtual bool canMakeToProjectiles(const sf::Vector2f location, const sf::Vector2f velocity, int divide_count) const{
        return false;
    }
private:

};

#endif	/* DIRTEATERPROJECTILE_HPP */

