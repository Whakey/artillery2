#include "Terrain.hpp"
#include <math.h>
#include <random>
#include <vector>
#include <iostream>

#define GROUNDCOLOR 56, 140, 36, 255

Terrain::Terrain(sf::Vector2i size) : size(size) {
	image.create(size.x, size.y);
	texture.loadFromImage(image);
}

bool Terrain::terrainFromFile(const std::string &filename) {
	return image.loadFromFile(filename);
}

void Terrain::triangleFill() {
	for (unsigned int i = 0; i != image.getSize().x; i++) {
		for (unsigned int j = 0; j != image.getSize().y; j++) {
			if (i < (int)(image.getSize().x / 2)) {
				if (j >= image.getSize().x - i) {
					image.setPixel(i, j, sf::Color(GROUNDCOLOR));
				}
				else {
					image.setPixel(i, j, sf::Color(0, 0, 0, 0));
				}
			}
			else {
				if (j <= image.getSize().x - i) {
					image.setPixel(i, j, sf::Color(GROUNDCOLOR));
				}
				else {
					image.setPixel(i, j, sf::Color(0, 0, 0, 0));
				}
			}
		}
	}
}

void Terrain::generateTerrain(int segments, int maxdisplace) {
	std::uniform_int_distribution<> rng(1, maxdisplace); 
	std::uniform_int_distribution<> rngInit((int)(0.35*size.y), (int)(0.5*size.y)); //RNG for the first value calculated, has different range than the other RNG

	std::mt19937 gen ((unsigned int)time(NULL));

	std::vector<double> X, Y;
	int segmentWidth = (int)size.x / segments;

	for (int i = 0; i < segments+1; i++) {
		X.push_back(i*segmentWidth);
		if (i == 0) {
			//First value is randomized separately
			int displace = rngInit(gen);
			double temp = (double) size.y - displace;
			Y.push_back(temp);
		}
		else {
			double displace = rng(gen);
			if (rng(gen) > 0.5*maxdisplace) { //50% go upwards, 50% go downwards
				if (Y[i - 1] - displace < 0.1*size.y) { //dont go too close to edges of the map
					Y.push_back(Y[i - 1] + displace);
				}
				else {
					Y.push_back(Y[i - 1] - displace);
				}
			}
			else {
				if (Y[i - 1] + displace > 0.9*size.y) {
					Y.push_back(Y[i - 1] - displace);
				}
				else {
					Y.push_back(Y[i - 1] + displace);
				}
			}
		}
	}

	//Calculate cubic spline interpolation of the random values, so we can get values at each point.
	tk::spline s;
	s.set_points(X, Y);
	std::vector <sf::Vector2i> displacedLine;
	for (int x = 0; x < size.x; x++) {
		displacedLine.push_back(sf::Vector2i(x, (int)s(x)));
	}


	for (int x = 0; x < size.x; x++) {
		for (int y = size.y - 1; y-- > 0; ) {
			if (y < displacedLine[x].y) {
				image.setPixel(x, y, sf::Color(0, 0, 0, 0));
			}
			else {
				image.setPixel(x, y, sf::Color(GROUNDCOLOR));
			}
		}
	}
}

bool Terrain::isGround(sf::Vector2i pos) {
	// All nonwhite pixels are ground
	if (pos.x < 0 || pos.y < 0) {
		return false;
	}
	if (image.getPixel(pos.x, pos.y) != sf::Color(0, 0, 0, 0)) {
		return true;
	}
	else {
		return false;
	}
}

sf::Vector2i Terrain::terrainHeight(sf::Vector2i pos) {
	// Assumes terrain is refreshed (i.e. no floating ground)
	if (isGround(pos)) {
		int i = pos.y;
		while (isGround(sf::Vector2i(pos.x, i)) && i > 0) {
			i--;
		}
		if (i > 0) {
			return sf::Vector2i(pos.x, i - 1);
		}
		else {
			return sf::Vector2i(pos.x, 0);
		}
	}
	else {
		int i = pos.y;
		while (!isGround(sf::Vector2i(pos.x, i)) && i < size.y) {
			i++;
		}
		if (i < size.y) {
			return sf::Vector2i(pos.x, i);
		}
		else {
			return sf::Vector2i(pos.x, size.y);
		}
	}
}

void Terrain::refresh() {
	for (int x = 0; x < size.x; x++) {
		for (int y = size.y - 1; y-- > 0; ) {
			if (image.getPixel(x, y) != sf::Color(0, 0, 0, 0)) {
				for (int i = size.y - 1; i >= y; i--) {
					if (image.getPixel(x, i) == sf::Color(0, 0, 0, 0)) {
						image.setPixel(x, y, sf::Color(0, 0, 0, 0));
						image.setPixel(x, i, sf::Color(GROUNDCOLOR));
						break;
					}
				}
			}
		}
	}
}

void Terrain::refresh(sf::Vector2i explpos, int explradius) {

	int startx, endx;
	int starty;

	if (explpos.x < explradius) {
		startx = 0;
		endx = explpos.x + explradius;
	}
	else if (explpos.x + explradius > size.x) {
		startx = explpos.x - explradius;
		endx = size.x;
	}
	else {
		startx = explpos.x - explradius;
		endx = explpos.x + explradius;
	}

	if (explpos.y < explradius) {
		starty = size.y - 1;
	}
	if (explpos.y + explradius > size.y) {
		starty = size.y - 1;
	}
	else {
		starty = explpos.y + explradius;
	}

	for (int x = startx; x < endx; x++) {
		for (int y = starty; y-- > 0; ) {
			if (image.getPixel(x, y) != sf::Color(0, 0, 0, 0)) {
				for (int i = size.y - 1; i >= y; i--) {
					if (image.getPixel(x, i) == sf::Color(0, 0, 0, 0)) {
						image.setPixel(x, y, sf::Color(0, 0, 0, 0));
						image.setPixel(x, i, sf::Color(GROUNDCOLOR));
						break;
					}
				}
			}
		}
	}
}

int distanceBetweenPoints(sf::Vector2i a, sf::Vector2i b) {
	int distance = (int)sqrt(pow(((double)a.x - b.x), 2.0) + pow(((double)a.y - b.y), 2.0));
	return distance;
}

void Terrain::explosion(sf::Vector2i pos, int radius) {

	int startx, endx;
	int starty, endy;

	startx = pos.x - radius;
	endx = pos.x + radius;
	starty = pos.y - radius;
	endy = pos.y + radius;

	if (startx < 0) {
		startx = 0;
	}
	if (endx > size.x) {
		endx = size.x;
	}
	if (starty < 0) {
		starty = 0;
	}
	if (starty > size.y) {
		starty = size.y;
	}

	for (int x = startx; x < endx; x++) {
		for (int y = starty; y < endy; y++) {
			if (distanceBetweenPoints(pos, sf::Vector2i(x, y)) < radius) {
				if (x < size.x - 1 && y < size.y - 1) {
					image.setPixel(x, y, sf::Color(0, 0, 0, 0));
				}
			}
		}
	}
}

void Terrain::updateSprite() {
	image.createMaskFromColor(sf::Color(0, 0, 0, 0));
	texture.update(image);
	sprite.setTexture(texture);
}

sf::Sprite const& Terrain::getSprite() const {
	return sprite;
}

sf::Image const& Terrain::getImage() const {
	return image;
}
