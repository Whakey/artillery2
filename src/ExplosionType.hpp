/* 
 * File:   ExplosionType.hpp
 * Author: iivari
 *
 * Created on November 12, 2015, 10:49 PM
 */

#ifndef EXPLOSIONTYPE_HPP
#define	EXPLOSIONTYPE_HPP

#include "Stronghold.hpp"
#include <iostream>

class ExplosionType {
public:
    ExplosionType(int);
//    ExplosionType(const ExplosionType& orig);
//   virtual ~ExplosionType();


    int getLenghtInFrames() {
        return lenght_in_frames;
    }
    
    void setExplosionColor(sf::Color color){
        explosion_color = color;
    }
    
    sf::Color getExplosionColor() const{
        return explosion_color;
    }

    virtual int simulateExplosionDamage(sf::Vector2f, Stronghold, float) const = 0;
    virtual void makeDamage(sf::Vector2f, std::vector<Stronghold>&, float) = 0;


private:
    int lenght_in_frames;
    sf::Color explosion_color;
};

#endif	/* EXPLOSIONTYPE_HPP */

