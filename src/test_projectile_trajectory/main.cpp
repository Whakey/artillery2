/* 
 * File:   main.cpp
 * Author: iivari
 *
 * Created on November 10, 2015, 1:56 AM
 */


#include <SFML/Graphics/Texture.hpp>
#include <SFML/System.hpp>

#include "../TrajectoryManager.hpp"
#include "../FlyingProjectiles.hpp"
#include "../BasicProjectile.hpp"
#include "../ExplosionType.hpp"
#include "../BasicExplosion.hpp"
#include "../DividingProjectile.hpp"
#include "../Terrain.hpp"
#include "../RainProjectile.hpp"
#include "../Shop.hpp"
#include "../SoundList.hpp"
#include <iostream>
#include <memory>
#include <iostream>
#include <memory>

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

int main() {
    float wind = -1;
    sf::Vector2i size(1000, 1000);
    Terrain background(size);
    background.triangleFill();
    background.refresh();
    SoundList voicelist = SoundList();

    FlyingProjectiles f_projectiles(size.y, voicelist);
    TrajectoryManager tr(wind, size.y, background);
    
    std::shared_ptr<ProjectileItem> bp= std::make_shared<BasicProjectile>();
    std::shared_ptr<ProjectileItem> dp= std::make_shared<DividingProjectile>();

    f_projectiles.addProjectile(sf::Vector2f(50, 50), sf::Vector2f(10, 20), *bp, 0);
    f_projectiles.addProjectile(sf::Vector2f(100, 20), sf::Vector2f(15, 3), *dp, 0);

    for (int i = 0; i < 1000; i++) {
        tr.UpdateProjectiles(f_projectiles);
        for (auto it : f_projectiles.getFlyingProjectiles()) {
            std::cout << "elapsed time: " << i / 60 << " y: " << it->getPosition().y << "  x: " << it->getPosition().x << "  y_vel: " << it->getVelocity().y << "  x_vel: " << it->getVelocity().x << " Name: " << it->getType().getName() << std::endl;
        }
        std::cout << "next frame" << std::endl;
    }

    return 0;
}

