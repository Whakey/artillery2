/* 
 * File:   Stronghold.hpp
 * Author: iivari
 *
 * Created on November 29, 2015, 11:58 PM
 */

#ifndef STRONGHOLD_HPP
#define	STRONGHOLD_HPP

#include "Player.hpp"
#include <SFML/Graphics.hpp>

class Stronghold {
public:
    Stronghold(sf::Vector2i location, Player &owner);

    sf::Vector2i getLocation() const {
        return location;
    }

    bool isDestroyed() const {
        return destroyed;
    }
    //bool detectCollision(Projectile);

    Player& getOwner() const{
        return owner;
    }

    int getMaxthealth() const {
        return maxhealth;
    }

    int getHealth() const {
        return health;
    }

    float getScale() const {
        return scale;
    }

    sf::Sprite getStrongholdSprite() {
        makeToCastle();
        return castle;
    }

    void setLocation(sf::Vector2i loc) {
        location = loc;
    }

    void update();

    void updateLocation(sf::Vector2i loc) {
        if (loc.y - location.y > 3)
            location = loc;
    }

    void augmentMaxhealth(int amounth);
    bool heathChange(int change);

    void makeToCastle();

private:
    int maxhealth;
    int health;
    sf::Vector2i location;
    bool destroyed = false;
    unsigned int height = 50;
    unsigned int width = 100;
    sf::Sprite castle;
    sf::Texture unbroken_t;
    sf::Texture broken_t;
    Player& owner;
    float scale = 0.15;

};

#endif	/* STRONGHOLD_HPP */

