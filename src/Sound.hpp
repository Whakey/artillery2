/* 
 * File:   Sound.hpp
 * Author: hopsale
 *
 * Created on November 11, 2015, 8:54 PM
 */

#ifndef SOUND_HPP
#define	SOUND_HPP

#include <SFML/Audio.hpp>
#include <string>
#include <iostream>
#include <memory>


enum SoundType { UNKNOWN, EXPLOSIONBIG, EXPLOSIONSMALL, EXPLOSION, EXPLOSION2, EXPLOSION3, EXPLOSION4,
  EXPLOSION5, SHOOT, SHOOT2, SHOOT3, SHOOT4, SHOOT5, SHOOT6, SHOOT7, RANDOM, CLICK, ERROR, PURCHASE,
  START, WINNING };
// Possible incoming sounds: POINT, BUTTON, AMMO

class Sound {
public:
    
  Sound(std::string filename, SoundType type = UNKNOWN, std::string location = "");
  
  int getRepeat() const {
    return repeat;
  }
  
  SoundType getType() const {
    return type;
  }
  
  std::string getLocation() const {
    return location;
  }
  
  float getLength() const {
    return length;
  }
  
  sf::SoundSource::Status getStatus() const {
      return sound.getStatus();
  }
  
  bool isStopped() const {
      return (sound.getStatus() == sf::SoundSource::Stopped);
  }
  
  bool isPlaying() const {
      return (sound.getStatus() == sf::SoundSource::Playing);
  }
  
  bool isPaused() const {
      return (sound.getStatus() == sf::SoundSource::Paused);
  }
  
  float getPitch() const {
      return sound.getPitch();
  }
  
  void setPitch(float pitch) {
      sound.setPitch(pitch);
  }
  
  void resetPitch() {
      sound.setPitch(1);
  }
  
  float getVolume() const {
      return sound.getVolume();
  }
  
  void setVolume(float volume) {
      sound.setVolume(volume);
  }
  
  void resetVolume() {
      sound.setVolume(100);
  }
  
  void play() {
      sound.play();
  }
  
  void stop() {
      sound.stop();
  }
  
  void pause() {
      sound.pause();
  }

  
private:
  sf::SoundBuffer buffer;
  sf::Sound sound;
  int repeat;
  SoundType type;
  std::string location;
  float length; //seconds
};

#endif	/* SOUND_HPP */

