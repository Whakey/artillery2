#ifndef GAMEOVER_HPP
#define GAMEOVER_HPP
#include "Appstates.hpp"

class GameOver : public AppStates
{
    public:
        GameOver();
        void renew();
        void draw(sf::RenderWindow &win);
        void pass(sf::Event Event);
        void update(float deltat);

    private:
        sf::Texture bground;
        sf::Sprite bgsprite;
        sf::Text gameovertext;
		sf::Text results;
};

#endif // GAMEOVER_HPP
